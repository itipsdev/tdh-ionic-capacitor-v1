

## Pages


### welcome


## scan-doc


## view-scanned-submit


## doc


## docs


## help


## scan-link


## settings


https://ionicframework.com/docs/native/barcode-scanner

https://github.com/phonegap/phonegap-plugin-barcodescanner

```bash

# Install plugin itself
npm install phonegap-plugin-barcodescanner

# npm install cordova-plugin-statusbar Incompatible


# Install wrapper for plugin
npm install @ionic-native/barcode-scanner



```


https://capacitor.ionicframework.com/docs/apis/splash-screen/#hiding-the-splash-screen   


[log] - Ionic Native: deviceready event fired after 1818 ms 
  
[warn] - Native: tried calling StatusBar.styleDefault, but the StatusBar plugin is not installed.   
[warn] - Install the StatusBar plugin: 'ionic cordova plugin add cordova-plugin-statusbar'   

[warn] - Native: tried calling SplashScreen.hide, but the SplashScreen plugin is not installed.   
[warn] - Install the SplashScreen plugin: 'ionic cordova plugin add cordova-plugin-splashscreen'   



## Android Graphic Assets

https://support.google.com/googleplay/android-developer/answer/1078870?hl=en


### Screenshots

Up to 8 screenshots for each
Phone
Tablet 7
Tablet 10

To publish your Store Listing, 
you must provide a minimum of 2 screenshots.

JPEG or 24-bit PNG (no alpha)
Minimum dimension: 320px
Maximum dimension: 3840px
The maximum dimension of your screenshot can't be more than twice as long as the minimum dimension.


### Icon

To publish your store listing, a high-res icon is required. 
The high-res icon does not replace your app's launcher icon, 
but should be a higher-fidelity, 
higher-resolution version that follows these design guidelines: 

Google Play icon design specifications
Material icons used through Android M
Adaptive icons for Android O
Requirements
32-bit PNG (with alpha)
Dimensions: 512px by 512px
Maximum file size: 1024KB


### Feature graphic

To publish an app and have it be featured anywhere on Google Play, a feature graphic is required. 
Your feature graphic is a powerful tool to show off your creative assets and attract new users.
Important: To display a feature graphic on your app’s store listing, you also need to add a promo video.

Requirements
JPEG or 24-bit PNG (no alpha)
Dimensions: 1024px by 500px


### Promo graphic

The promo graphic is used for promotions on older versions of the Android OS (earlier than 4.0). 
This image is not required to submit an update for your Store Listing.

Requirements
JPG or 24-bit PNG (no alpha)
Dimensions: 180px by 120px




https://www.storemaven.com/app-store-product-page-promotional-artwork-requirements/


Generate Service


```

ionic generate service ServiceName


```

icu4c is keg-only, which means it was not symlinked into /usr/local,
because macOS provides libicucore.dylib (but nothing else).

If you need to have icu4c first in your PATH run:
  echo 'export PATH="/usr/local/opt/icu4c/bin:$PATH"' >> ~/.bash_profile
  echo 'export PATH="/usr/local/opt/icu4c/sbin:$PATH"' >> ~/.bash_profile

For compilers to find icu4c you may need to set:
  export LDFLAGS="-L/usr/local/opt/icu4c/lib"
  export CPPFLAGS="-I/usr/local/opt/icu4c/include"


node@10

node@10 is keg-only, which means it was not symlinked into /usr/local,
because this is an alternate version of another formula.

If you need to have node@10 first in your PATH run:
```
  echo 'export PATH="/usr/local/opt/node@10/bin:$PATH"' >> ~/.bash_profile
```

For compilers to find node@10 you may need to set:
```
  export LDFLAGS="-L/usr/local/opt/node@10/lib"
  export CPPFLAGS="-I/usr/local/opt/node@10/include"

```
  
