import {Component, OnInit} from '@angular/core';
import {GlobalDataService} from '../../services/global-data.service';
import {TaxDocListItem} from '../../models/TaxDocListItem';
import {Observable} from 'rxjs';
import {LoggerService} from '../../services/logger.service';

@Component({
    selector: 'app-docs',
    templateUrl: './docs.component.html',
    styleUrls: ['./docs.component.scss'],
})
export class DocsComponent implements OnInit {

    isLoading: boolean = false;

    taxDocs$: Observable<TaxDocListItem[]>;

    constructor(
        private global: GlobalDataService,
        private logger: LoggerService
    ) {

    }

    ngOnInit() {

        this.logger.trace('DocsComponent ngOnInit');

        this.taxDocs$ = this.global.taxDocuments$;

    }

    refreshList() {

    }

}
