import { Component, Input, OnInit } from '@angular/core';
import { DescriptionValue } from '../../models/descriptionValue';
import { TaxDocListItem } from '../../models/TaxDocListItem';
import { GlobalDataService } from '../../services/global-data.service';
import {LoggerService} from '../../services/logger.service';

@Component({
  selector: 'app-doc-content',
  templateUrl: './doc-content.component.html',
  styleUrls: ['./doc-content.component.scss'],
})
export class DocContentComponent implements OnInit {

  @Input()
  public docData: TaxDocListItem;

  public docId: string;

  public simpleItems: Array<DescriptionValue> = new Array<DescriptionValue>();

  constructor(
      private global: GlobalDataService,
      private logger: LoggerService
  ) {

  }

  ngOnInit(

  ) {

    this.logger.trace( 'DocContentComponent ngOnInit begin' );

    this.logger.debugAny( this.docData );

    this.docId = this.docData.id;

    this.logger.dump( 'docId', this.docId );

    this.logger.trace( 'DocContentComponent ngOnInit end' );

  }

}
