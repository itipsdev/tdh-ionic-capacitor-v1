import { Component, OnDestroy, OnInit } from '@angular/core';
import { TaxDocListItem } from '../../models/TaxDocListItem';
import { GlobalDataService } from '../../services/global-data.service';
import { ActivatedRoute } from '@angular/router';
import {LoggerService} from '../../services/logger.service';

@Component({
  selector: 'app-doc-facsimile',
  templateUrl: './doc-facsimile.component.html',
  styleUrls: ['./doc-facsimile.component.scss'],
})
export class DocFacsimileComponent implements OnInit, OnDestroy {

  // Index of this document in document list
  public docId: number;

  public doc: TaxDocListItem;

  constructor(
      private global: GlobalDataService,
      private activatedRoute: ActivatedRoute,
      private logger: LoggerService
  ) {


  }

  ngOnInit( ) {

    // Subscribe ?
    this.docId = this.activatedRoute.snapshot.params['docId'];

    this.logger.trace( `DocComponent ngOnInit ${this.docId}` );

    const arr: TaxDocListItem[] = this.global.getTaxDocs( );

    this.logger.debugAny( arr );

    this.doc = arr[ this.docId ];

    this.logger.debugAny( this.doc );

  }

  ngOnDestroy( ): void {

  }

}
