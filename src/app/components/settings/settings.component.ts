import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalDataService } from '../../services/global-data.service';
import { Observable } from 'rxjs';
import { LoggerService } from '../../services/logger.service';

@Component( {
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: [ './settings.component.scss' ],
} )
export class SettingsComponent implements OnInit {

    deviceGuid$: Observable<string>;

    lockerNumber$: Observable<string>;

    specialUseCode: string = '';

    // Not yet supported 2019 10 11 BVW
    public showLink: boolean = false;

    constructor(
        private router: Router,
        private logger: LoggerService,
        private global: GlobalDataService
    ) {

    }

    ngOnInit( ) {

        this.logger.trace( 'SettingsComponent ngOnInit' );

        this.deviceGuid$ = this.global.deviceGuid$;

        this.lockerNumber$ = this.global.lockerNumber$;

    }

    onClear( ) {

        this.global.clearTaxDocs( );

    }

    onClickLinkToLocker( ) {

        this.logger.trace( 'SettingsComponent onClickLinkToLocker' );

        this.router.navigate( [ '/scan-link' ] );

    }

    toggleTestMode( ) {

        this.logger.trace( 'SettingsComponent toggleTestMode' );

        this.global.toggleTestMode( );

    }

    turnOffTestMode( ) {

        this.logger.trace( 'SettingsComponent turnOffTestMode' );

        this.global.turnOffTestMode( );

    }

}
