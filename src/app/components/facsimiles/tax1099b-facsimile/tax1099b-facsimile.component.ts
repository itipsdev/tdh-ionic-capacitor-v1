import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099B } from '../../../models/tax1099B';


@Component({
    selector: 'app-tax1099b-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099b.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099BFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 719;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 719 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099B;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 16 );

        // 1  Payer's name, etc
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.payerNameAddress ), 8, 79 );

        // 2  Payer's federal identification number
        this.placeTextCenter( this.doc.payerTin, 127, 307 );

        // 3  Recipient's identification number
        this.placeTextCenter( this.doc.recipientTin, 372, 307 );

        // 4  Recipient's name
        this.placeText( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 379 );

        // 5  Recipient's street address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 451 );

        // 6  Recipient's city, state and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 523 );

        // 7  Account number
        this.placeTextCenter( this.doc.accountNumber, 243, 571 );

        // 8  CUSIP number
        this.placeTextCenter( this.formatter.format3( 'cusip', this.doc.securityDetails, 1 ), 171, 619 );

        // 9  FATCA filing requirement
        this.placeText( this.formatter.format3( 'foreignAccountTaxCompliance', this.doc.securityDetails, 1 ), 454, 615 );

        // 10 14 State name
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 1 ), 78, 667 );

        // 11  State name 2
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 2 ), 78, 691 );

        // 12 15 State identification number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 1 ), 235, 667 );

        // 13  State identification number 2
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 2 ), 235, 691 );

        // 14 16 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 485, 667 );

        // 15  State tax withheld 2
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 2 ), 485, 691 );

        // 16  Applicable check box on Form 8949
        this.placeTextCenter( this.formatter.format3( 'checkboxOnForm8949', this.doc.securityDetails, 1 ), 617, 115 );

        // 17 1a Description of property
        this.placeText( this.formatter.format3( 'saleDescription', this.doc.securityDetails, 1 ), 495, 163 );

        // 18 1b Date acquired
        this.placeTextCenter( this.formatter.format3( 'dateAcquired', this.doc.securityDetails, 1 ), 588, 211 );

        // 19 1c Date sold or disposed
        this.placeTextCenter( this.formatter.format3( 'dateOfSale', this.doc.securityDetails, 1 ), 790, 211 );

        // 20 1d Proceeds
        this.placeTextRight( this.formatter.format3( 'salesPrice', this.doc.securityDetails, 1 ), 687, 259 );

        // 21 1e Cost or other basis
        this.placeTextRight( this.formatter.format3( 'costBasis', this.doc.securityDetails, 1 ), 889, 259 );

        // 22 1f Accrued market discount
        this.placeTextRight( this.formatter.format3( 'accruedMarketDiscount', this.doc.securityDetails, 1 ), 687, 307 );

        // 23 1g Wash sale loss disallowed
        this.placeTextRight( this.formatter.format3( 'washSaleLossDisallowed', this.doc.securityDetails, 1 ), 889, 307 );

        // 24 2 Short-term gain or loss
        this.placeText( this.formatter.format5( 'longOrShort', 'SHORT', this.doc.securityDetails, 1, 1 ), 666, 327 );

        // 25 2 Long-term gain or loss
        this.placeText( this.formatter.format5( 'longOrShort', 'LONG', this.doc.securityDetails, 1, 1 ), 666, 351 );

        // 26 2 Ordinary
        this.placeTextCenter( this.formatter.format3( 'ordinary', this.doc.securityDetails, 1 ), 668, 375 );

        // 27 3 Collectibles
        this.placeTextCenter( this.formatter.format3( 'collectible', this.doc.securityDetails, 1 ), 870, 351 );

        // 28 3 Qualified Opportunity Fund (QOF)
        this.placeTextCenter( this.formatter.format3( 'qof', this.doc.securityDetails, 1 ), 870, 375 );

        // 29 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 687, 427 );

        // 30 5 If checked, noncovered security
        this.placeTextCenter( this.formatter.format3( 'noncoveredSecurity', this.doc.securityDetails, 1 ), 859, 423 );

        // 31 6 Reported to IRS - Gross proceeds
        this.placeTextCenter( this.formatter.format5( 'grossOrNet', 'GROSS', this.doc.securityDetails, 1, 1 ), 668, 471 );

        // 32 6 Reported to IRS - Net proceeds
        this.placeTextCenter( this.formatter.format5( 'grossOrNet', 'NET', this.doc.securityDetails, 1, 1 ), 668, 495 );

        // 33 7 If checked, loss is not allowed based on amount in 1d
        this.placeText( this.formatter.format3( 'lossNotAllowed', this.doc.securityDetails, 1 ), 857, 495 );

        // 34 8 Profit or (loss) realized in 2019 on closed contracts
        this.placeTextRight( this.formatter.format( 'number', this.doc.profitOnClosedContracts ), 687, 571 );

        // 35 9 Unrealized profit or loss on open contracts - 12/31/2018
        this.placeTextRight( this.formatter.format( 'number', this.doc.unrealizedProfitOpenContractsBegin ), 889, 571 );

        // 36 10 Unrealized profit or loss on open contracts - 12/31/2019
        this.placeTextRight( this.formatter.format( 'number', this.doc.unrealizedProfitOpenContractsEnd ), 687, 643 );

        // 37 11 Aggregate profit or (loss) on contracts
        this.placeTextRight( this.formatter.format( 'number', this.doc.aggregateProfitOnContracts ), 889, 643 );

        // 38 12 If checked, basis reported to IRS
        this.placeText( this.formatter.format3( 'basisReported', this.doc.securityDetails, 1 ), 656, 687 );

        // 39 13 Bartering
        this.placeTextRight( this.formatter.format( 'number', this.doc.bartering ), 889, 691 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}