import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1120SK1 } from '../../../models/tax1120SK1';


@Component({
    selector: 'app-tax1120sk1-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1120ssk.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1120SK1FacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1415;

    trimWidth: number = 1082;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1415 / 1082;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1120SK1;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1082;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  Final K-1
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.finalK1 ), 583, 39 );

        // 1  Amended K-1
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.amendedK1 ), 741, 39 );

        // 2  Beginning month
        this.placeText( '', 135, 163 );

        // 3  Beginning day
        this.placeText( '', 184, 163 );

        // 4  Beginning year
        this.placeText( '', 227, 163 );

        // 5  Ending month
        this.placeText( '', 379, 163 );

        // 6  Ending day
        this.placeText( '', 428, 163 );

        // 7  Ending year
        this.placeText( '', 472, 163 );

        // 8 A Corporation's employer identification number
        this.placeTextCenter( this.doc.corporationTin, 273, 331 );

        // 9 B Corporation's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.corporationNameAddress ), 7, 379 );

        // 10 C IRS Center where corporation filed return
        this.placeTextCenter( this.doc.irsCenter, 274, 523 );

        // 11 D Shareholder's identifying number
        this.placeTextCenter( this.doc.shareholderTin, 273, 619 );

        // 12 E Shareholder's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.shareholderNameAddress ), 7, 667 );

        // 13 F Shareholder's percentage of stock ownership for tax year
        this.placeTextRight( this.formatter.format( 'number', this.doc.percentOwnership ), 506, 811 );

        // 14 1 Ordinary business income (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryIncome ), 818, 139 );

        // 15 2 Net rental real estate income (loss
        this.placeTextRight( this.formatter.format( 'number', this.doc.netRentalRealEstateIncome ), 818, 187 );

        // 16 3 Other net rental income (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherRentalIncome ), 818, 235 );

        // 17 4 Interest income
        this.placeTextRight( this.formatter.format( 'number', this.doc.interestIncome ), 818, 283 );

        // 18 5a Ordinary dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryDividends ), 818, 331 );

        // 19 5b Qualified dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedDividends ), 818, 379 );

        // 20 6 Royalties
        this.placeTextRight( this.formatter.format( 'number', this.doc.royalties ), 818, 427 );

        // 21 7 Net short-term capital gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.netShortTermGain ), 818, 475 );

        // 22 8a Net long-term capital gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.netLongTermGain ), 818, 523 );

        // 23 8b Collectibles (28%) gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.collectiblesGain ), 818, 571 );

        // 24 8c Unrecaptured section 1250 gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.unrecaptured1250Gain ), 818, 619 );

        // 25 9 Net section 1231 gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.net1231Gain ), 818, 667 );

        // 26 10 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 1 ), 568, 715 );

        // 27 10 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 1 ), 818, 715 );

        // 28 10 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 2 ), 568, 763 );

        // 29 10 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 2 ), 818, 763 );

        // 30 10 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 3 ), 568, 811 );

        // 31 10 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 3 ), 818, 811 );

        // 32 10 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 4 ), 568, 859 );

        // 33 10 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 4 ), 818, 859 );

        // 34 10 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 5 ), 568, 907 );

        // 35 10 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 5 ), 818, 907 );

        // 36 11 Section 179 deduction
        this.placeTextRight( this.formatter.format( 'number', this.doc.section179Deduction ), 818, 955 );

        // 37 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 1 ), 568, 1003 );

        // 38 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 1 ), 818, 1003 );

        // 39 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 2 ), 568, 1051 );

        // 40 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 2 ), 818, 1051 );

        // 41 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 3 ), 568, 1099 );

        // 42 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 3 ), 818, 1099 );

        // 43 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 4 ), 568, 1147 );

        // 44 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 4 ), 818, 1147 );

        // 45 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 5 ), 568, 1195 );

        // 46 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 5 ), 818, 1195 );

        // 47 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 6 ), 568, 1243 );

        // 48 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 6 ), 818, 1243 );

        // 49 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 7 ), 568, 1291 );

        // 50 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 7 ), 818, 1291 );

        // 51 12 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 8 ), 568, 1339 );

        // 52 12 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 8 ), 818, 1339 );

        // 53 13 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 1 ), 841, 139 );

        // 54 13 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 1 ), 1077, 139 );

        // 55 13 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 2 ), 841, 187 );

        // 56 13 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 2 ), 1077, 187 );

        // 57 13 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 3 ), 841, 235 );

        // 58 13 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 3 ), 1077, 235 );

        // 59 13 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 4 ), 841, 283 );

        // 60 13 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 4 ), 1077, 283 );

        // 61 13 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 5 ), 841, 331 );

        // 62 13 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 5 ), 1077, 331 );

        // 63 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeIf', this.doc.foreignCountry, 'A' ), 841, 379 );

        // 64 14 Foreign transactions
        this.placeTextRight( this.formatter.format( 'Text', this.doc.foreignCountry ), 1077, 379 );

        // 65 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 1 ), 841, 427 );

        // 66 14 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 1 ), 1077, 427 );

        // 67 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 2 ), 841, 475 );

        // 68 14 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 2 ), 1077, 475 );

        // 69 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 3 ), 841, 523 );

        // 70 14 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 3 ), 1077, 523 );

        // 71 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 4 ), 841, 571 );

        // 72 14 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 4 ), 1077, 571 );

        // 73 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 5 ), 841, 619 );

        // 74 14 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 5 ), 1077, 619 );

        // 75 14 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 6 ), 841, 667 );

        // 76 14 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 6 ), 1077, 667 );

        // 77 15 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 1 ), 841, 715 );

        // 78 15 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 1 ), 1077, 715 );

        // 79 15 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 2 ), 841, 763 );

        // 80 15 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 2 ), 1077, 763 );

        // 81 15 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 3 ), 841, 811 );

        // 82 15 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 3 ), 1077, 811 );

        // 83 15 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 4 ), 841, 859 );

        // 84 15 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 4 ), 1077, 859 );

        // 85 15 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 5 ), 841, 907 );

        // 86 15 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 5 ), 1077, 907 );

        // 87 16 Items affecting shareholder basis
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.basisItems, 1 ), 841, 955 );

        // 88 16 Items affecting shareholder basis
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.basisItems, 1 ), 1077, 955 );

        // 89 16 Items affecting shareholder basis
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.basisItems, 2 ), 841, 1003 );

        // 90 16 Items affecting shareholder basis
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.basisItems, 2 ), 1077, 1003 );

        // 91 16 Items affecting shareholder basis
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.basisItems, 3 ), 841, 1051 );

        // 92 16 Items affecting shareholder basis
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.basisItems, 3 ), 1077, 1051 );

        // 93 16 Items affecting shareholder basis
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.basisItems, 4 ), 841, 1099 );

        // 94 16 Items affecting shareholder basis
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.basisItems, 4 ), 1077, 1099 );

        // 95 16 Items affecting shareholder basis
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.basisItems, 5 ), 841, 1147 );

        // 96 16 Items affecting shareholder basis
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.basisItems, 5 ), 1077, 1147 );

        // 97 17 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 1 ), 841, 1195 );

        // 98 17 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 1 ), 1077, 1195 );

        // 99 17 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 2 ), 841, 1243 );

        // 100 17 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 2 ), 1077, 1243 );

        // 101 17 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 3 ), 841, 1291 );

        // 102 17 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 3 ), 1077, 1291 );

        // 103 17 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 4 ), 841, 1339 );

        // 104 17 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 4 ), 1077, 1339 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}