import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1065K1 } from '../../../models/tax1065K1';


@Component({
    selector: 'app-tax1065k1-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1065sk1.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1065K1FacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1487;

    trimWidth: number = 1083;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1487 / 1083;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1065K1;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1083;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  Tax year beginning month
        this.placeText( '', 149, 163 );

        // 1  Tax year beginning day
        this.placeText( '', 198, 163 );

        // 2  Tax year ending month
        this.placeText( '', 379, 163 );

        // 3  Tax year ending day
        this.placeText( '', 428, 163 );

        // 4  Tax year ending year
        this.placeText( '', 472, 163 );

        // 5  Final K-1
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.finalK1 ), 583, 39 );

        // 6  Amended K-1
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.amendedK1 ), 741, 39 );

        // 7 A Partnership's employer identification number
        this.placeTextCenter( this.doc.partnershipTin, 273, 307 );

        // 8 B Partnership's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.partnershipNameAddress ), 9, 355 );

        // 9 C IRS Center where partnership filed return
        this.placeTextCenter( this.doc.irsCenter, 275, 475 );

        // 10 D Check if this is a publicly traded partnership (PTP)
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.publiclyTraded ), 50, 495 );

        // 11 E Partner's identifying number
        this.placeTextCenter( this.doc.partnerTin, 273, 595 );

        // 12 F Partner's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.partnerNameAddress ), 9, 643 );

        // 13 G General partner or LLC member-manager
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.generalPartner ), 50, 735 );

        // 14 G Limited partner or other LLC member
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.limitedPartner ), 295, 735 );

        // 15 H Domestic partner
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.domestic ), 50, 783 );

        // 16 H Foreign partner
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.foreign ), 295, 783 );

        // 17 I1 What type of entity is this partner?
        this.placeText( this.doc.entityType, 280, 835 );

        // 18 I2 If this partner is a retirement plan (IRA/SEP/Keogh/etc.), check here
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.retirementPlan ), 483, 855 );

        // 19 J Partner's share of profit - beginning
        this.placeTextRight( this.formatter.format( 'number', this.doc.profitShareBegin ), 287, 931 );

        // 20 J Partner's share of profit - ending
        this.placeTextRight( this.formatter.format( 'number', this.doc.profitShareEnd ), 503, 931 );

        // 21 J Partner's share of loss - beginning
        this.placeTextRight( this.formatter.format( 'number', this.doc.lossShareBegin ), 287, 955 );

        // 22 J Partner's share of loss - ending
        this.placeTextRight( this.formatter.format( 'number', this.doc.lossShareEnd ), 503, 955 );

        // 23 J Partner's share of capital - beginning
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalShareBegin ), 287, 979 );

        // 24 J Partner's share of capital - ending
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalShareEnd ), 503, 979 );

        // 25 K Partner's share of liabilities - beginning - nonrecourse
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonrecourseLiabilityShareBegin ), 358, 1075 );

        // 26 K Partner's share of liabilities - ending - nonrecourse
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonrecourseLiabilityShareEnd ), 532, 1075 );

        // 27 K Partner's share of liabilities - beginning - qualified nonrecourse financing
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedLiabilityShareBegin ), 358, 1123 );

        // 28 K Partner's share of liabilities - ending - qualified nonrecourse financing
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedLiabilityShareEnd ), 532, 1123 );

        // 29 K Partner's share of liabilities - beginning - recourse
        this.placeTextRight( this.formatter.format( 'number', this.doc.recourseLiabilityShareBegin ), 358, 1147 );

        // 30 K Partner's share of liabilities - ending - recourse
        this.placeTextRight( this.formatter.format( 'number', this.doc.recourseLiabilityShareEnd ), 532, 1147 );

        // 31 L Partner's capital account analysis - Beginning capital account
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalAccountBegin ), 532, 1195 );

        // 32 L Partner's capital account analysis - Capital contributed during the year
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalAccountContributions ), 532, 1219 );

        // 33 L Partner's capital account analysis - Current year increase (decrease)
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalAccountIncrease ), 532, 1243 );

        // 34 L Partner's capital account analysis - Withdrawals & distributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalAccountWithdrawals ), 524, 1267 );

        // 35 L Partner's capital account analysis - Ending capital account
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalAccountEnd ), 532, 1291 );

        // 36 L Tax basis
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.bookTax ), 50, 1335 );

        // 37 L GAAP
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.bookGaap ), 194, 1335 );

        // 38 L Section 704(b) book
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.book704b ), 309, 1335 );

        // 39 L Other (explain)
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.bookOther ), 50, 1359 );

        // 40 L Other (explain)
        this.placeText( this.doc.bookOtherExplain, 178, 1387 );

        // 41 M Did the partner contribute property with a built-in gain or loss? - Yes
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.builtInGain ), 50, 1431 );

        // 42 M Did the partner contribute property with a built-in gain or loss? - No
        this.placeTextCenter( '', 194, 1431 );

        // 43  
        this.placeText( '', 552, 139 );

        // 44 1 Ordinary business income (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryIncome ), 818, 139 );

        // 45  
        this.placeText( '', 552, 187 );

        // 46 2 Net rental real estate income (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.netRentalRealEstateIncome ), 818, 187 );

        // 47  
        this.placeText( '', 552, 235 );

        // 48 3 Other net rental income (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherRentalIncome ), 818, 235 );

        // 49  
        this.placeText( '', 552, 283 );

        // 50 4 Guaranteed payments
        this.placeTextRight( this.formatter.format( 'number', this.doc.guaranteedPayment ), 818, 283 );

        // 51  
        this.placeText( '', 552, 331 );

        // 52 5 Interest income
        this.placeTextRight( this.formatter.format( 'number', this.doc.interestIncome ), 818, 331 );

        // 53  
        this.placeText( '', 552, 379 );

        // 54 6a Ordinary dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryDividends ), 818, 379 );

        // 55  
        this.placeText( '', 552, 427 );

        // 56 6b Qualified dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedDividends ), 818, 427 );

        // 57  
        this.placeText( '', 552, 475 );

        // 58 6c Dividend equivalents
        this.placeTextRight( this.formatter.format( 'number', this.doc.dividendEquivalents ), 818, 475 );

        // 59  
        this.placeText( '', 552, 523 );

        // 60 7 Royalties
        this.placeTextRight( this.formatter.format( 'number', this.doc.royalties ), 818, 523 );

        // 61  
        this.placeText( '', 552, 571 );

        // 62 8 Net short-term capital gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.netShortTermGain ), 818, 571 );

        // 63  
        this.placeText( '', 552, 619 );

        // 64 9a Net long-term capital gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.netLongTermGain ), 818, 619 );

        // 65  
        this.placeText( '', 552, 667 );

        // 66 9b Collectibles (28%) gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.collectiblesGain ), 818, 667 );

        // 67  
        this.placeText( '', 552, 715 );

        // 68 9c Unrecaptured section 1250 gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.unrecaptured1250Gain ), 818, 715 );

        // 69  
        this.placeText( '', 552, 763 );

        // 70 10 Net section 1231 gain (loss)
        this.placeTextRight( this.formatter.format( 'number', this.doc.net1231Gain ), 818, 763 );

        // 71 11 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 1 ), 568, 811 );

        // 72 11 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 1 ), 818, 811 );

        // 73 11 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 2 ), 568, 859 );

        // 74 11 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 2 ), 818, 859 );

        // 75 11 Other income (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherIncome, 3 ), 568, 907 );

        // 76 11 Other income (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherIncome, 3 ), 818, 907 );

        // 77 12 Section 179 deduction
        this.placeText( '', 552, 955 );

        // 78 12 Section 179 deduction
        this.placeTextRight( this.formatter.format( 'number', this.doc.section179Deduction ), 818, 955 );

        // 79 13 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 1 ), 568, 1003 );

        // 80 13 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 1 ), 818, 1003 );

        // 81 13 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 2 ), 568, 1051 );

        // 82 13 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 2 ), 818, 1051 );

        // 83 13 Other deductions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherDeductions, 3 ), 568, 1099 );

        // 84 13 Other deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherDeductions, 3 ), 818, 1099 );

        // 85 14 Self-employment earnings (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.selfEmployment, 1 ), 568, 1147 );

        // 86 14 Self-employment earnings (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.selfEmployment, 1 ), 818, 1147 );

        // 87 14 Self-employment earnings (loss)
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.selfEmployment, 2 ), 568, 1195 );

        // 88 14 Self-employment earnings (loss)
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.selfEmployment, 2 ), 818, 1195 );

        // 89 15 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 1 ), 841, 139 );

        // 90 15 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 1 ), 1075, 139 );

        // 91 15 Credits
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.credits, 2 ), 841, 187 );

        // 92 15 Credits
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 2 ), 1075, 187 );

        // 93 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeIf', this.doc.foreignCountry, 'A' ), 841, 235 );

        // 94 16 Foreign transactions
        this.placeTextRight( this.formatter.format( 'Ttext', this.doc.foreignCountry  ), 1075, 235 );

        // 95 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 1 ), 841, 283 );

        // 96 16 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 1 ), 1075, 283 );

        // 97 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 2 ), 841, 331 );

        // 98 16 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 2 ), 1075, 331 );

        // 99 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 3 ), 841, 379 );

        // 100 16 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 3 ), 1075, 379 );

        // 101 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 4 ), 841, 427 );

        // 102 16 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 4 ), 1075, 427 );

        // 103 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 5 ), 841, 475 );

        // 104 16 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 5 ), 1075, 475 );

        // 105 16 Foreign transactions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.foreignTransactions, 6 ), 841, 523 );

        // 106 16 Foreign transactions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.foreignTransactions, 6 ), 1075, 523 );

        // 107 17 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 1 ), 841, 571 );

        // 108 17 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 1 ), 1075, 571 );

        // 109 17 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 2 ), 841, 619 );

        // 110 17 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 2 ), 1075, 619 );

        // 111 17 Alternative minimum tax (AMT) items
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.amtItems, 3 ), 841, 667 );

        // 112 17 Alternative minimum tax (AMT) items
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtItems, 3 ), 1075, 667 );

        // 113 18 Tax-exempt income and nondeductible expenses
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.taxExemptIncome, 1 ), 841, 739 );

        // 114 18 Tax-exempt income and nondeductible expenses
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.taxExemptIncome, 1 ), 1075, 739 );

        // 115 18 Tax-exempt income and nondeductible expenses
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.taxExemptIncome, 2 ), 841, 787 );

        // 116 18 Tax-exempt income and nondeductible expenses
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.taxExemptIncome, 2 ), 1075, 787 );

        // 117 18 Tax-exempt income and nondeductible expenses
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.taxExemptIncome, 3 ), 841, 835 );

        // 118 18 Tax-exempt income and nondeductible expenses
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.taxExemptIncome, 3 ), 1075, 835 );

        // 119 19 Distributions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.distributions, 1 ), 841, 883 );

        // 120 19 Distributions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.distributions, 1 ), 1075, 883 );

        // 121 19 Distributions
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.distributions, 2 ), 841, 931 );

        // 122 19 Distributions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.distributions, 2 ), 1075, 931 );

        // 123 20 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 1 ), 841, 1003 );

        // 124 20 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 1 ), 1075, 1003 );

        // 125 20 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 2 ), 841, 1051 );

        // 126 20 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 2 ), 1075, 1051 );

        // 127 20 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 3 ), 841, 1099 );

        // 128 20 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 3 ), 1075, 1099 );

        // 129 20 Other information
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 4 ), 841, 1147 );

        // 130 20 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 4 ), 1075, 1147 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}