import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Oid } from '../../../models/tax1099Oid';


@Component({
    selector: 'app-tax1099oid-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099oid.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099OidFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 695;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 695 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099Oid;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0 $field.boxNumber $field.boxDescription
        this.placeText( '', 393, 16 );

        // 1  Payer's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 83 );

        // 2  Payer's TIN
        this.placeTextCenter( this.doc.payerTin, 121, 307 );

        // 3  Recipient's TIN
        this.placeTextCenter( this.doc.recipientTin, 365, 307 );

        // 4  Recipient's name
        this.placeText( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 379 );

        // 5  Recipient's street address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 451 );

        // 6  Recipient's city, state, and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 523 );

        // 7  FATCA filing requirement
        this.placeText( this.formatter.format( 'Boolean', this.doc.foreignAccountTaxCompliance ), 440, 583 );

        // 8  Account number
        this.placeTextCenter( this.doc.accountNumber, 244, 667 );

        // 9 1 Original issue discount
        this.placeTextRight( this.formatter.format( 'number', this.doc.originalIssueDiscount ), 687, 91 );

        // 10 2 Other periodic interest
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherPeriodicInterest ), 687, 211 );

        // 11 3 Early withdrawal penalty
        this.placeTextRight( this.formatter.format( 'number', this.doc.earlyWithdrawalPenalty ), 687, 259 );

        // 12 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 889, 259 );

        // 13 5 Market discount
        this.placeTextRight( this.formatter.format( 'number', this.doc.marketDiscount ), 687, 331 );

        // 14 6 Acquisition premium
        this.placeTextRight( this.formatter.format( 'number', this.doc.acquisitionPremium ), 889, 331 );

        // 15 7 Description
        this.placeParagraph( this.doc.description, 495, 379 );

        // 16 8 Original issue discount on U.S. Treasury obligations
        this.placeTextRight( this.formatter.format( 'number', this.doc.discountOnTreasuryObligations ), 687, 523 );

        // 17 9 Investment expenses
        this.placeTextRight( this.formatter.format( 'number', this.doc.investmentExpenses ), 889, 523 );

        // 18 10 Bond premium
        this.placeTextRight( this.formatter.format( 'number', this.doc.bondPremium ), 687, 595 );

        // 19 11 Tax-exempt OID
        this.placeTextRight( this.formatter.format( 'number', this.doc.taxExemptOid ), 889, 595 );

        // 20 12 State
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 1 ), 516, 643 );

        // 22 13 State identification number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 1 ), 631, 643 );

        // 24 14 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 889, 643 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}