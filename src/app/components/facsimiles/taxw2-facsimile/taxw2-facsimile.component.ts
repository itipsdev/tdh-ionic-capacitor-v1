import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { TaxW2 } from '../../../models/taxW2';


@Component({
    selector: 'app-taxw2-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/fw2.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class TaxW2FacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 700;

    trimWidth: number = 1084;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 700 / 1084;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: TaxW2;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1084;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  Employee's social security number
        this.placeTextCenter( this.doc.employeeTin, 359, 42 );

        // 1  Employer identification number (EIN)
        this.placeTextCenter( this.doc.employerTin, 294, 90 );

        // 2  Employer's name, address, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.employerNameAddress ), 9, 138 );

        // 3  Control number
        this.placeTextCenter( this.doc.controlNumber, 294, 282 );

        // 4  Employee's first name and initial
        this.placeText( this.formatter.format( 'FirstInit', this.doc.employeeName ), 9, 330 );

        // 5  Last name
        this.placeText( this.formatter.format( 'LastName', this.doc.employeeName ), 239, 330 );

        // 6  Suff.
        this.placeText( this.formatter.format( 'Suffix', this.doc.employeeName ), 552, 330 );

        // 7  Employee's address and ZIP code
        this.placeParagraph( this.formatter.format( 'Address', this.doc.employeeAddress ), 9, 354 );

        // 8 1 Wages, tips, other compensation
        this.placeTextRight( this.formatter.format( 'number', this.doc.wages ), 832, 90 );

        // 9 2 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 1077, 90 );

        // 10 3 Social security wages
        this.placeTextRight( this.formatter.format( 'number', this.doc.socialSecurityWages ), 832, 138 );

        // 11 4 Social security tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.socialSecurityTaxWithheld ), 1077, 138 );

        // 12 5 Medicare wages and tips
        this.placeTextRight( this.formatter.format( 'number', this.doc.medicareWages ), 832, 186 );

        // 13 6 Medicare tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.medicareTaxWithheld ), 1077, 186 );

        // 14 7 Social security tips
        this.placeTextRight( this.formatter.format( 'number', this.doc.socialSecurityTips ), 832, 234 );

        // 15 8 Allocated tips
        this.placeTextRight( this.formatter.format( 'number', this.doc.allocatedTips ), 1077, 234 );

        // 16 9 Verification code
        this.placeTextCenter( '', 711, 282 );

        // 17 10 Dependent care benefits
        this.placeTextRight( this.formatter.format( 'number', this.doc.dependentCareBenefit ), 1077, 282 );

        // 18 11 Nonqualified plans
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonQualifiedPlan ), 832, 330 );

        // 19 12a Code
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.codes, 1 ), 877, 330 );

        // 20 12a Amount
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.codes, 1 ), 1077, 330 );

        // 21 12b Code
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.codes, 2 ), 877, 378 );

        // 22 12b Amount
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.codes, 2 ), 1077, 378 );

        // 23 12c Code
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.codes, 3 ), 877, 426 );

        // 24 12c Amount
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.codes, 3 ), 1077, 426 );

        // 25 12d Code
        this.placeTextCenter( this.formatter.format3( 'CodeLetter', this.doc.codes, 4 ), 877, 474 );

        // 26 12d Amount
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.codes, 4 ), 1077, 474 );

        // 27 13 Statutory employee
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.statutory ), 633, 375 );

        // 28 13 Retirement plan
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.retirementPlan ), 706, 375 );

        // 29 13 Third-party sick pay
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.thirdPartySickPay ), 778, 375 );

        // 30 14 Other
        this.placeParagraph( this.formatter.format3( 'OtherPara', this.doc.other, 20 ), 599, 426 );

        // 31 15 State
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 1 ), 28, 546 );

        // 32 15 Employer's state ID number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 1 ), 186, 546 );

        // 33 16 State wages, tips, etc.
        this.placeTextRight( this.formatter.format3( 'StateIncome', this.doc.stateTaxWithholding, 1 ), 487, 546 );

        // 34 17 State income tax
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 645, 546 );

        // 35 18 Local wages, tips, etc.
        this.placeTextRight( this.formatter.format3( 'LocalIncome', this.doc.localTaxWithholding, 1 ), 818, 546 );

        // 36 19 Local income tax
        this.placeTextRight( this.formatter.format3( 'LocalWH', this.doc.localTaxWithholding, 1 ), 976, 546 );

        // 37 20 Locality name
        this.placeText( this.formatter.format3( 'Locality', this.doc.localTaxWithholding, 1 ), 986, 546 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}