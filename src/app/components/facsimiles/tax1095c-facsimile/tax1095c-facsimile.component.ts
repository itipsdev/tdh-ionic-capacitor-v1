import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1095C } from '../../../models/tax1095C';


@Component({
    selector: 'app-tax1095c-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1095c.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1095CFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1099;

    trimWidth: number = 1442;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1099 / 1442;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1095C;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1442;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  VOID
        this.placeText( '', 1056, 66 );

        // 1  CORRECTED
        this.placeText( '', 1056, 115 );

        // 2 1 Name of employee - first name
        this.placeText( 'combo-x' /* getFirstName( employeeName ) */, 5, 209 );

        // 3  Name of employee - middle initial
        this.placeText( 'combo-x' /* getMiddleInitial( employeeName ) */, 207, 209 );

        // 4  Name of employee - last name
        this.placeText( 'combo-x' /* getLastName( employeeName ) */, 266, 209 );

        // 5 2 Social security number (SSN)
        this.placeText( this.doc.tin, 466, 209 );

        // 6 3 Street address (including apartment no.)
        this.placeText( 'combo-x' /* getStreetAddress( employeeAddress ) */, 5, 257 );

        // 7 4 City or town
        this.placeText( 'combo-x' /* getCity( employeeAddress ) */, 5, 305 );

        // 8 5 State or province
        this.placeText( 'combo-x' /* getState( employeeAddress ) */, 223, 305 );

        // 9 6 Country and ZIP or foreign postal code
        this.placeText( 'combo-x' /* getZip( employeeAddress ) */, 468, 305 );

        // 10 7 Name of employer
        this.placeText( 'combo-x' /* getName( employerNameAddressPhone ) */, 727, 209 );

        // 11 8 Employer identification number (EIN)
        this.placeText( this.doc.employerId, 1186, 209 );

        // 12 9 Street address (including room or suite no.)
        this.placeText( 'combo-x' /* getStreetAddress( employerNameAddressPhone ) */, 727, 257 );

        // 13 10 Contact telephone number
        this.placeText( 'combo-x' /* getPhone( employerNameAddressPhone ) */, 1186, 257 );

        // 14 11 City or town
        this.placeText( 'combo-x' /* getCity( employerNameAddressPhone ) */, 727, 305 );

        // 15 12 State or province
        this.placeText( 'combo-x' /* getState( employerNameAddressPhone ) */, 943, 305 );

        // 16 13 Country and ZIP or foreign postal code
        this.placeText( 'combo-x' /* getZip( employerNameAddressPhone ) */, 1188, 305 );

        // 17  Plan start month
        this.placeText( this.formatter.format( 'integer', this.doc.planStartMonth ), 1090, 329 );

        // 18 14 Offer of coverage (enter required code) - All 12 Months
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 13 ) */, 135, 401 );

        // 19 14 Offer of coverage (enter required code) - Jan
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 1 ) */, 235, 401 );

        // 20 14 Offer of coverage (enter required code) - Feb
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 2 ) */, 336, 401 );

        // 21 14 Offer of coverage (enter required code) - Mar
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 3 ) */, 437, 401 );

        // 22 14 Offer of coverage (enter required code) - Apr
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 4 ) */, 538, 401 );

        // 23 14 Offer of coverage (enter required code) - May
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 5 ) */, 639, 401 );

        // 24 14 Offer of coverage (enter required code) - Jun
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 6 ) */, 739, 401 );

        // 25 14 Offer of coverage (enter required code) - Jul
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 7 ) */, 840, 401 );

        // 26 14 Offer of coverage (enter required code) - Aug
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 8 ) */, 941, 401 );

        // 27 14 Offer of coverage (enter required code) - Sep
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 9 ) */, 1042, 401 );

        // 28 14 Offer of coverage (enter required code) - Oct
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 10 ) */, 1143, 401 );

        // 29 14 Offer of coverage (enter required code) - Nov
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 11 ) */, 1243, 401 );

        // 30 14 Offer of coverage (enter required code) - Dec
        this.placeText( 'combo-x' /* getOfferCode( offersOfCoverage, 12 ) */, 1344, 401 );

        // 31 15 Employee Required Contribution - All 12 Months
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 13 ) */, 146, 473 );

        // 32 15 Employee Required Contribution - Jan
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 1 ) */, 247, 473 );

        // 33 15 Employee Required Contribution - Feb
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 2 ) */, 347, 473 );

        // 34 15 Employee Required Contribution - Mar
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 3 ) */, 448, 473 );

        // 35 15 Employee Required Contribution - Apr
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 4 ) */, 549, 473 );

        // 36 15 Employee Required Contribution - May
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 5 ) */, 650, 473 );

        // 37 15 Employee Required Contribution - Jun
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 6 ) */, 751, 473 );

        // 38 15 Employee Required Contribution - Jul
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 7 ) */, 851, 473 );

        // 39 15 Employee Required Contribution - Aug
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 8 ) */, 952, 473 );

        // 40 15 Employee Required Contribution - Sep
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 9 ) */, 1053, 473 );

        // 41 15 Employee Required Contribution - Oct
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 10 ) */, 1154, 473 );

        // 42 15 Employee Required Contribution - Nov
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 11 ) */, 1255, 473 );

        // 43 15 Employee Required Contribution - Dec
        this.placeText( 'combo-x' /* getOfferContrib( offersOfCoverage, 12 ) */, 1355, 473 );

        // 44 16 Section 4980H Safe Harbor and Other Relief (enter code, if applicable)
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 13 ) */, 135, 545 );

        // 45 16 Section 4980H Safe Harbor and Other Relief - Jan
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 1 ) */, 235, 545 );

        // 46 16 Section 4980H Safe Harbor and Other Relief - Feb
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 2 ) */, 336, 545 );

        // 47 16 Section 4980H Safe Harbor and Other Relief - Mar
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 3 ) */, 437, 545 );

        // 48 16 Section 4980H Safe Harbor and Other Relief - Apr
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 4 ) */, 538, 545 );

        // 49 16 Section 4980H Safe Harbor and Other Relief - May
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 5 ) */, 639, 545 );

        // 50 16 Section 4980H Safe Harbor and Other Relief - Jun
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 6 ) */, 739, 545 );

        // 51 16 Section 4980H Safe Harbor and Other Relief - Jul
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 7 ) */, 840, 545 );

        // 52 16 Section 4980H Safe Harbor and Other Relief - Aug
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 8 ) */, 941, 545 );

        // 53 16 Section 4980H Safe Harbor and Other Relief - Sep
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 9 ) */, 1042, 545 );

        // 54 16 Section 4980H Safe Harbor and Other Relief - Oct
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 10 ) */, 1143, 545 );

        // 55 16 Section 4980H Safe Harbor and Other Relief - Nov
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 11 ) */, 1243, 545 );

        // 56 16 Section 4980H Safe Harbor and Other Relief - Dec
        this.placeText( 'combo-x' /* getOfferRelief( offersOfCoverage, 12 ) */, 1344, 545 );

        // 57  Check if employer provided self-insured coverage
        this.placeText( this.formatter.format( 'Boolean', this.doc.selfInsuredCoverage ), 1310, 587 );

        // 58 17(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 1 ) */, 36, 713 );

        // 59 17(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 1 ) */, 163, 713 );

        // 60 17(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 1 ) */, 223, 713 );

        // 61 17(b) SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 1 ) */, 351, 713 );

        // 62 17(c) DOB (if SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 1 ) */, 523, 713 );

        // 63 17(d) Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 1 ) */, 697, 689 );

        // 64 17(e) Months of Coverage - Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 1 ) */, 769, 691 );

        // 65 17(e) Months of Coverage - Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 2 ) */, 827, 691 );

        // 66 17(e) Months of Coverage - Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 3 ) */, 885, 691 );

        // 67 17(e) Months of Coverage - Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 4 ) */, 942, 691 );

        // 68 17(e) Months of Coverage - May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 5 ) */, 1000, 691 );

        // 69 17(e) Months of Coverage - Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 6 ) */, 1057, 691 );

        // 70 17(e) Months of Coverage - Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 7 ) */, 1115, 691 );

        // 71 17(e) Months of Coverage - Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 8 ) */, 1173, 691 );

        // 72 17(e) Months of Coverage - Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 9 ) */, 1230, 691 );

        // 73 17(e) Months of Coverage - Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 10 ) */, 1288, 691 );

        // 74 17(e) Months of Coverage - Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 11 ) */, 1345, 691 );

        // 75 17(e) Months of Coverage - Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 12 ) */, 1403, 691 );

        // 76 18(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 2 ) */, 36, 785 );

        // 77 18(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 2 ) */, 163, 785 );

        // 78 18(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 2 ) */, 223, 785 );

        // 79 18(b) SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 2 ) */, 351, 785 );

        // 80 18(c) DOB (if SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 2 ) */, 523, 785 );

        // 81 18(d) Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 2 ) */, 697, 761 );

        // 82 18(e) Months of Coverage - Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 1 ) */, 769, 763 );

        // 83 18(e) Months of Coverage - Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 2 ) */, 827, 763 );

        // 84 18(e) Months of Coverage - Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 3 ) */, 885, 763 );

        // 85 18(e) Months of Coverage - Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 4 ) */, 942, 763 );

        // 86 18(e) Months of Coverage - May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 5 ) */, 1000, 763 );

        // 87 18(e) Months of Coverage - Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 6 ) */, 1057, 763 );

        // 88 18(e) Months of Coverage - Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 7 ) */, 1115, 763 );

        // 89 18(e) Months of Coverage - Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 8 ) */, 1173, 763 );

        // 90 18(e) Months of Coverage - Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 9 ) */, 1230, 763 );

        // 91 18(e) Months of Coverage - Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 10 ) */, 1288, 763 );

        // 92 18(e) Months of Coverage - Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 11 ) */, 1345, 763 );

        // 93 18(e) Months of Coverage - Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 12 ) */, 1403, 763 );

        // 94 19(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 3 ) */, 36, 857 );

        // 95 19(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 3 ) */, 163, 857 );

        // 96 19(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 3 ) */, 223, 857 );

        // 97 19(b) SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 3 ) */, 351, 857 );

        // 98 19(c) DOB (if SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 3 ) */, 523, 857 );

        // 99 19(d) Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 3 ) */, 697, 833 );

        // 100 19(e) Months of Coverage - Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 1 ) */, 769, 835 );

        // 101 19(e) Months of Coverage - Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 2 ) */, 827, 835 );

        // 102 19(e) Months of Coverage - Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 3 ) */, 885, 835 );

        // 103 19(e) Months of Coverage - Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 4 ) */, 942, 835 );

        // 104 19(e) Months of Coverage - May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 5 ) */, 1000, 835 );

        // 105 19(e) Months of Coverage - Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 6 ) */, 1057, 835 );

        // 106 19(e) Months of Coverage - Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 7 ) */, 1115, 835 );

        // 107 19(e) Months of Coverage - Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 8 ) */, 1173, 835 );

        // 108 19(e) Months of Coverage - Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 9 ) */, 1230, 835 );

        // 109 19(e) Months of Coverage - Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 10 ) */, 1288, 835 );

        // 110 19(e) Months of Coverage - Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 11 ) */, 1345, 835 );

        // 111 19(e) Months of Coverage - Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 12 ) */, 1403, 835 );

        // 112 20(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 4 ) */, 36, 929 );

        // 113 20(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 4 ) */, 163, 929 );

        // 114 20(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 4 ) */, 223, 929 );

        // 115 20(b) SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 4 ) */, 351, 929 );

        // 116 20(c) DOB (if SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 4 ) */, 523, 929 );

        // 117 20(d) Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 4 ) */, 697, 905 );

        // 118 20(e) Months of Coverage - Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 1 ) */, 769, 907 );

        // 119 20(e) Months of Coverage - Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 2 ) */, 827, 907 );

        // 120 20(e) Months of Coverage - Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 3 ) */, 885, 907 );

        // 121 20(e) Months of Coverage - Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 4 ) */, 942, 907 );

        // 122 20(e) Months of Coverage - May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 5 ) */, 1000, 907 );

        // 123 20(e) Months of Coverage - Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 6 ) */, 1057, 907 );

        // 124 20(e) Months of Coverage - Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 7 ) */, 1115, 907 );

        // 125 20(e) Months of Coverage - Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 8 ) */, 1173, 907 );

        // 126 20(e) Months of Coverage - Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 9 ) */, 1230, 907 );

        // 127 20(e) Months of Coverage - Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 10 ) */, 1288, 907 );

        // 128 20(e) Months of Coverage - Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 11 ) */, 1345, 907 );

        // 129 20(e) Months of Coverage - Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 12 ) */, 1403, 907 );

        // 130 21(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 5 ) */, 36, 1001 );

        // 131 21(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 5 ) */, 163, 1001 );

        // 132 21(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 5 ) */, 223, 1001 );

        // 133 21(b) SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 5 ) */, 351, 1001 );

        // 134 21(c) DOB (if SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 5 ) */, 523, 1001 );

        // 135 21(d) Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 5 ) */, 697, 977 );

        // 136 21(e) Months of Coverage - Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 1 ) */, 769, 979 );

        // 137 21(e) Months of Coverage - Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 2 ) */, 827, 979 );

        // 138 21(e) Months of Coverage - Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 3 ) */, 885, 979 );

        // 139 21(e) Months of Coverage - Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 4 ) */, 942, 979 );

        // 140 21(e) Months of Coverage - May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 5 ) */, 1000, 979 );

        // 141 21(e) Months of Coverage - Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 6 ) */, 1057, 979 );

        // 142 21(e) Months of Coverage - Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 7 ) */, 1115, 979 );

        // 143 21(e) Months of Coverage - Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 8 ) */, 1173, 979 );

        // 144 21(e) Months of Coverage - Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 9 ) */, 1230, 979 );

        // 145 21(e) Months of Coverage - Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 10 ) */, 1288, 979 );

        // 146 21(e) Months of Coverage - Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 11 ) */, 1345, 979 );

        // 147 21(e) Months of Coverage - Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 12 ) */, 1403, 979 );

        // 148 22(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 6 ) */, 36, 1073 );

        // 149 22(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 6 ) */, 163, 1073 );

        // 150 22(a) Name of covered individual(s)
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 6 ) */, 223, 1073 );

        // 151 22(b) SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 6 ) */, 351, 1073 );

        // 152 22(c) DOB (if SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 6 ) */, 523, 1073 );

        // 153 22(d) Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 6 ) */, 697, 1049 );

        // 154 22(e) Months of Coverage - Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 1 ) */, 769, 1051 );

        // 155 22(e) Months of Coverage - Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 2 ) */, 827, 1051 );

        // 156 22(e) Months of Coverage - Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 3 ) */, 885, 1051 );

        // 157 22(e) Months of Coverage - Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 4 ) */, 942, 1051 );

        // 158 22(e) Months of Coverage - May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 5 ) */, 1000, 1051 );

        // 159 22(e) Months of Coverage - Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 6 ) */, 1057, 1051 );

        // 160 22(e) Months of Coverage - Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 7 ) */, 1115, 1051 );

        // 161 22(e) Months of Coverage - Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 8 ) */, 1173, 1051 );

        // 162 22(e) Months of Coverage - Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 9 ) */, 1230, 1051 );

        // 163 22(e) Months of Coverage - Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 10 ) */, 1288, 1051 );

        // 164 22(e) Months of Coverage - Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 11 ) */, 1345, 1051 );

        // 165 22(e) Months of Coverage - Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 12 ) */, 1403, 1051 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}