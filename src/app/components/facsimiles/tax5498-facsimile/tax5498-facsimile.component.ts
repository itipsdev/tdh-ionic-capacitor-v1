import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax5498 } from '../../../models/tax5498';


@Component({
    selector: 'app-tax5498-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f5498.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax5498FacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 695;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 695 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax5498;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 16 );

        // 1  TRUSTEE'S or ISSUER'S name, street address, city or town, state or province, country, and ZIP or foreign postal code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.issuerNameAddress ), 8, 83 );

        // 2  TRUSTEE's or ISSUER'S TIN
        this.placeTextCenter( this.doc.issuerTin, 121, 307 );

        // 3  PARTICIPANT's TIN
        this.placeTextCenter( this.doc.participantTin, 365, 307 );

        // 4  PARTICIPANT's name
        this.placeText( this.formatter.format( 'Name', this.doc.participantNameAddress ), 8, 379 );

        // 5  PARTICIPANT's street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.participantNameAddress ), 8, 451 );

        // 6  PARTICIPANT's city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.participantNameAddress ), 8, 523 );

        // 7  Account number
        this.placeTextCenter( this.doc.accountNumber, 244, 667 );

        // 8 1 IRA contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.iraContributions ), 687, 115 );

        // 9 2 Rollover contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.rolloverContributions ), 687, 163 );

        // 10 3 Roth IRA conversion amount
        this.placeTextRight( this.formatter.format( 'number', this.doc.rothIraConversion ), 687, 235 );

        // 11 4 Recharacterized contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.recharacterizedContributions ), 889, 235 );

        // 12 5 Fair market value of account
        this.placeTextRight( this.formatter.format( 'number', this.doc.fairMarketValue ), 687, 307 );

        // 13 6 Life insurance cost included in box 1
        this.placeTextRight( this.formatter.format( 'number', this.doc.lifeInsuranceCost ), 889, 307 );

        // 14 7a IRA
        this.placeText( this.formatter.format( 'Boolean', this.doc.ira ), 558, 327 );

        // 15 7b SEP
        this.placeText( this.formatter.format( 'Boolean', this.doc.sep ), 645, 327 );

        // 16 7c SIMPLE
        this.placeText( this.formatter.format( 'Boolean', this.doc.simple ), 762, 327 );

        // 17 7d ROTHIRA
        this.placeText( this.formatter.format( 'Boolean', this.doc.rothIra ), 872, 327 );

        // 18 8 SEP contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.sepContributions ), 687, 379 );

        // 19 9 SIMPLE contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.simpleContributions ), 889, 379 );

        // 20 10 Roth IRA contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.rothIraContributions ), 687, 427 );

        // 21 11 If checked, required minimum distribution for 2020
        this.placeText( this.formatter.format( 'Boolean', this.doc.rmdNextYear ), 873, 423 );

        // 22 12a RMD date
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.rmdDate ), 589, 475 );

        // 23 12b RMD amount
        this.placeTextRight( this.formatter.format( 'number', this.doc.rmdAmount ), 889, 475 );

        // 24 13a Postponed contribution
        this.placeTextRight( this.formatter.format( 'number', this.doc.postponedContribution ), 687, 523 );

        // 25 13b Year
        this.placeText( this.formatter.format( 'integer', this.doc.postponedYear ), 695, 523 );

        // 26 13c Code
        this.placeTextCenter( this.doc.postponedCode, 825, 523 );

        // 27 14a Repayments
        this.placeTextRight( this.formatter.format( 'number', this.doc.repayments ), 687, 595 );

        // 28 14b Code
        this.placeTextCenter( this.doc.repayCode, 790, 595 );

        // 29 15a FMV of certain specified assets
        this.placeTextRight( this.formatter.format( 'number', this.doc.fmvSpecifiedAssets ), 687, 667 );

        // 30 15b Code(s)
        this.placeTextCenter( this.doc.specifiedCodes, 790, 667 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}