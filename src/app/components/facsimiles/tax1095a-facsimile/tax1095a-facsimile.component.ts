import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1095A } from '../../../models/tax1095A';


@Component({
    selector: 'app-tax1095a-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1095a.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1095AFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1463;

    trimWidth: number = 1082;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1463 / 1082;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1095A;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1082;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  VOID
        this.placeText( '', 768, 19 );

        // 1  CORRECTED
        this.placeText( '', 768, 68 );

        // 2 1 Marketplace identifier
        this.placeText( this.doc.marketplaceId, 5, 186 );

        // 3 2 Marketplace-assigned policy number
        this.placeText( this.doc.marketplacePolicyNumber, 307, 186 );

        // 4 3 Policy issuer's name
        this.placeText( this.doc.policyIssuerName, 597, 186 );

        // 5 4 Recipient's name
        this.placeText( this.doc.recipientName, 5, 234 );

        // 6 5 Recipient's SSN
        this.placeText( this.doc.recipientTin, 595, 234 );

        // 7 6 Recipient's date of birth
        this.placeText( this.formatter.format( 'Timestamp', this.doc.recipientDateOfBirth ), 840, 234 );

        // 8 7 Recipient's spouse's name
        this.placeText( this.doc.spouseName, 5, 282 );

        // 9 8 Recipient's spouse's SSN
        this.placeText( this.doc.spouseTin, 595, 282 );

        // 10 9 Recipient's spouse's date of birth
        this.placeText( this.formatter.format( 'Timestamp', this.doc.spouseDateOfBirth ), 840, 282 );

        // 11 10 Policy start date
        this.placeText( this.formatter.format( 'Timestamp', this.doc.policyStartDate ), 5, 330 );

        // 12 11 Policy termination date
        this.placeText( this.formatter.format( 'Timestamp', this.doc.policyTerminationDate ), 307, 330 );

        // 13 12 Street address (including apartment no.)
        this.placeText( 'combo-x' /* getStreetAddress( recipientAddress ) */, 597, 330 );

        // 14 13 City or town
        this.placeText( 'combo-x' /* getCity( recipientAddress ) */, 5, 378 );

        // 15 14 State or province
        this.placeText( 'combo-x' /* getState( recipientAddress ) */, 307, 378 );

        // 16 15 Country and ZIP or foreign postal code
        this.placeText( 'combo-x' /* getZip( recipientAddress ) */, 597, 378 );

        // 17 16A Covered individual name
        this.placeText( 'combo-x' /* getMktCoveredIndividualName( coveredIndividuals, 1 ) */, 36, 522 );

        // 18 16B Covered individual SSN
        this.placeText( 'combo-x' /* getMktCoveredIndividualTin( coveredIndividuals, 1 ) */, 394, 522 );

        // 19 16C Covered individual date of birth
        this.placeText( 'combo-x' /* getMktCoveredIndividualDob( coveredIndividuals, 1 ) */, 567, 522 );

        // 20 16D Coverage start date
        this.placeText( 'combo-x' /* getMktCoveredIndividualStart( coveredIndividuals, 1 ) */, 739, 522 );

        // 21 16E Coverage termination date
        this.placeText( 'combo-x' /* getMktCoveredIndividualTerm( coveredIndividuals, 1 ) */, 912, 522 );

        // 22 17A Covered individual name
        this.placeText( 'combo-x' /* getMktCoveredIndividualName( coveredIndividuals, 2 ) */, 36, 570 );

        // 23 17B Covered individual SSN
        this.placeText( 'combo-x' /* getMktCoveredIndividualTin( coveredIndividuals, 2 ) */, 394, 570 );

        // 24 17C Covered individual date of birth
        this.placeText( 'combo-x' /* getMktCoveredIndividualDob( coveredIndividuals, 2 ) */, 567, 570 );

        // 25 17D Coverage start date
        this.placeText( 'combo-x' /* getMktCoveredIndividualStart( coveredIndividuals, 2 ) */, 739, 570 );

        // 26 17E Coverage termination date
        this.placeText( 'combo-x' /* getMktCoveredIndividualTerm( coveredIndividuals, 2 ) */, 912, 570 );

        // 27 18A Covered individual name
        this.placeText( 'combo-x' /* getMktCoveredIndividualName( coveredIndividuals, 3 ) */, 36, 618 );

        // 28 18B Covered individual SSN
        this.placeText( 'combo-x' /* getMktCoveredIndividualTin( coveredIndividuals, 3 ) */, 394, 618 );

        // 29 18C Covered individual date of birth
        this.placeText( 'combo-x' /* getMktCoveredIndividualDob( coveredIndividuals, 3 ) */, 567, 618 );

        // 30 18D Coverage start date
        this.placeText( 'combo-x' /* getMktCoveredIndividualStart( coveredIndividuals, 3 ) */, 739, 618 );

        // 31 18E Coverage termination date
        this.placeText( 'combo-x' /* getMktCoveredIndividualTerm( coveredIndividuals, 3 ) */, 912, 618 );

        // 32 19A Covered individual name
        this.placeText( 'combo-x' /* getMktCoveredIndividualName( coveredIndividuals, 4 ) */, 36, 666 );

        // 33 19B  Covered individual SSN
        this.placeText( 'combo-x' /* getMktCoveredIndividualTin( coveredIndividuals, 4 ) */, 394, 666 );

        // 34 19C Covered individual date of birth
        this.placeText( 'combo-x' /* getMktCoveredIndividualDob( coveredIndividuals, 4 ) */, 567, 666 );

        // 35 19D Coverage start date
        this.placeText( 'combo-x' /* getMktCoveredIndividualStart( coveredIndividuals, 4 ) */, 739, 666 );

        // 36 19E Coverage termination date
        this.placeText( 'combo-x' /* getMktCoveredIndividualTerm( coveredIndividuals, 4 ) */, 912, 666 );

        // 37 20A Covered individual name
        this.placeText( 'combo-x' /* getMktCoveredIndividualName( coveredIndividuals, 5 ) */, 36, 714 );

        // 38 20B  Covered individual SSN
        this.placeText( 'combo-x' /* getMktCoveredIndividualTin( coveredIndividuals, 5 ) */, 394, 714 );

        // 39 20C Covered individual date of birth
        this.placeText( 'combo-x' /* getMktCoveredIndividualDob( coveredIndividuals, 5 ) */, 567, 714 );

        // 40 20D Coverage start date
        this.placeText( 'combo-x' /* getMktCoveredIndividualStart( coveredIndividuals, 5 ) */, 739, 714 );

        // 41 20E Coverage termination date
        this.placeText( 'combo-x' /* getMktCoveredIndividualTerm( coveredIndividuals, 5 ) */, 912, 714 );

        // 42 21A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 1 ) */, 250, 858 );

        // 43 21B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 1 ) */, 495, 858 );

        // 44 21C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 1 ) */, 783, 858 );

        // 45 22A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 2 ) */, 250, 906 );

        // 46 22B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 2 ) */, 495, 906 );

        // 47 22C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 2 ) */, 783, 906 );

        // 48 23A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 3 ) */, 250, 954 );

        // 49 23B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 3 ) */, 495, 954 );

        // 50 23C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 3 ) */, 783, 954 );

        // 51 24A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 4 ) */, 250, 1002 );

        // 52 24B  Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 4 ) */, 495, 1002 );

        // 53 24C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 4 ) */, 783, 1002 );

        // 54 25A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 5 ) */, 250, 1050 );

        // 55 25B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 5 ) */, 495, 1050 );

        // 56 25C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 5 ) */, 783, 1050 );

        // 57 26A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 6 ) */, 250, 1098 );

        // 58 26B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 6 ) */, 495, 1098 );

        // 59 26C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 6 ) */, 783, 1098 );

        // 60 27A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 7 ) */, 250, 1146 );

        // 61 27B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 7 ) */, 495, 1146 );

        // 62 27C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 7 ) */, 783, 1146 );

        // 63 28A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 8 ) */, 250, 1194 );

        // 64 28B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 8 ) */, 495, 1194 );

        // 65 28C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 8 ) */, 783, 1194 );

        // 66 29A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 9 ) */, 250, 1242 );

        // 67 29B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 9 ) */, 495, 1242 );

        // 68 29C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 9 ) */, 783, 1242 );

        // 69 30A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 10 ) */, 250, 1290 );

        // 70 30B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 10 ) */, 495, 1290 );

        // 71 30C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 10 ) */, 783, 1290 );

        // 72 31A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 11 ) */, 250, 1338 );

        // 73 31B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 11 ) */, 495, 1338 );

        // 74 31C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 11 ) */, 783, 1338 );

        // 75 32A Monthly enrollment premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 12 ) */, 250, 1386 );

        // 76 32B Monthly second lowest cost silver plan (SLCSP) premium
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 12 ) */, 495, 1386 );

        // 77 32C Monthly advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 12 ) */, 783, 1386 );

        // 78 33A Total plan premiums
        this.placeText( 'combo-x' /* getCoveragePrem( coverages, 13 ) */, 250, 1434 );

        // 79 33B Total plan premiums sedond lowest cost silver plan
        this.placeText( 'combo-x' /* getCoverageSL( coverages, 13 ) */, 495, 1434 );

        // 80 33C Total advance payment of premium tax credit
        this.placeText( 'combo-x' /* getCoverageAdv( coverages, 13 ) */, 783, 1434 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}