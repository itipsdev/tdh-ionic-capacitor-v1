import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Int } from '../../../models/tax1099Int';


@Component({
    selector: 'app-tax1099int-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099int.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099IntFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 695;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 695 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099Int;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 16 );

        // 1  Payer's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 83 );

        // 2  Payer's TIN
        this.placeTextCenter( this.doc.payerTin, 121, 307 );

        // 3  Recipient's TIN
        this.placeTextCenter( this.doc.recipientTin, 365, 307 );

        // 4  Recipient's name
        this.placeText( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 379 );

        // 5  Recipient's address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 451 );

        // 6  Recipient's city, state, and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 523 );

        // 7  FATCA filing requirement
        this.placeText( this.formatter.format( 'Boolean', this.doc.foreignAccountTaxCompliance ), 440, 583 );

        // 8  Account number
        this.placeTextCenter( this.doc.accountNumber, 244, 667 );

        // 9  Payer's RTN
        this.placeTextCenter( this.doc.payerRtn, 588, 91 );

        // 10 1 Interest income
        this.placeTextRight( this.formatter.format( 'number', this.doc.interestIncome ), 687, 163 );

        // 11 2 Early withdrawal penalty
        this.placeTextRight( this.formatter.format( 'number', this.doc.earlyWithdrawalPenalty ), 889, 235 );

        // 12 3 Interest on U.S. Savings Bonds and Treasury obligations
        this.placeTextRight( this.formatter.format( 'number', this.doc.usBondInterest ), 889, 307 );

        // 13 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 687, 355 );

        // 14 5 Investment expenses
        this.placeTextRight( this.formatter.format( 'number', this.doc.investmentExpenses ), 889, 355 );

        // 15 6 Foreign tax paid
        this.placeTextRight( this.formatter.format( 'number', this.doc.foreignTaxPaid ), 687, 403 );

        // 16 7 Foreign country or U.S. possession
        this.placeTextCenter( this.doc.foreignCountry, 790, 403 );

        // 17 8 Tax-exempt interest
        this.placeTextRight( this.formatter.format( 'number', this.doc.taxExemptInterest ), 687, 475 );

        // 18 9 Specified private activity bond interest
        this.placeTextRight( this.formatter.format( 'number', this.doc.specifiedPabInterest ), 889, 475 );

        // 19 10 Market discount
        this.placeTextRight( this.formatter.format( 'number', this.doc.marketDiscount ), 687, 547 );

        // 20 11 Bond premium
        this.placeTextRight( this.formatter.format( 'number', this.doc.bondPremium ), 889, 547 );

        // 21 12 Bond premium on Treasury obligations
        this.placeTextRight( this.formatter.format( 'number', this.doc.usBondPremium ), 687, 595 );

        // 22 13 Bond premium on tax-exempt bond
        this.placeTextRight( this.formatter.format( 'number', this.doc.taxExemptBondPremium ), 889, 595 );

        // 23 14 Tax-exempt bond CUSIP no.
        this.placeTextCenter( this.doc.cusipNumber, 588, 667 );

        // 24 15 State
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 1 ), 718, 643 );

        // 25 16 State
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 2 ), 718, 667 );

        // 26 17 State identification number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 1 ), 819, 643 );

        // 27 15 State identification number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 2 ), 819, 667 );

        // 28 16 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 1049, 643 );

        // 29 17 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 2 ), 1049, 667 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}