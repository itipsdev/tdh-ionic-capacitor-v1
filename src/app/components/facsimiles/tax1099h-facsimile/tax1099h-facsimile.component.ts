import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099H } from '../../../models/tax1099H';


@Component({
    selector: 'app-tax1099h-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099h.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099HFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 454;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 454 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099H;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 16 );

        // 1  ISSUER'S/PROVIDER'S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.issuerNameAddress ), 8, 83 );

        // 2  ISSUER'S/PROVIDER'S federal identification number
        this.placeTextCenter( this.doc.issuerTin, 120, 187 );

        // 3  RECIPIENT'S identification number
        this.placeTextCenter( this.doc.recipientTin, 365, 187 );

        // 4  RECIPIENT'S name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 231 );

        // 5  RECIPIENT'S street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 331 );

        // 6  RECIPIENT'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 379 );

        // 7 1 Amount of HCTC advance payments
        this.placeTextRight( this.formatter.format( 'number', this.doc.advancePayments ), 688, 67 );

        // 8 2 Number of months HCTC advance payments and reimbursement credits paid to you
        this.placeTextCenter( this.formatter.format( 'integer', this.doc.numberOfMonths ), 588, 139 );

        // 9 3 January
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'JAN' ), 688, 187 );

        // 10 4 February
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'FEB' ), 688, 235 );

        // 11 5 March
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'MAR' ), 688, 283 );

        // 12 6 April
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'APR' ), 688, 331 );

        // 13 7 May
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'MAY' ), 688, 379 );

        // 14 8 June
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'JUN' ), 688, 427 );

        // 15 9 July
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'JUL' ), 889, 187 );

        // 16 10 August
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'AUG' ), 889, 235 );

        // 17 11 September
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'SEP' ), 889, 283 );

        // 18 12 October
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'OCT' ), 889, 331 );

        // 19 13 November
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'NOV' ), 889, 379 );

        // 20 14 December
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.payments, 'DEC' ), 889, 427 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}