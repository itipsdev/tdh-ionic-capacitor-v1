import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098 } from '../../../models/tax1098';


@Component({
    selector: 'app-tax1098-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1098.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1098FacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 694;

    trimWidth: number = 1056;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 694 / 1056;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1098;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1056;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeTextCenter( '', 396, 15 );

        // 1  RECIPIENT'S/LENDER'S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.lenderNameAddress ), 8, 91 );

        // 2  RECIPIENT'S/LENDER'S TIN
        this.placeTextCenter( this.doc.lenderTin, 117, 307 );

        // 3  PAYER'S/BORROWER'S TIN
        this.placeTextCenter( this.doc.borrowerTin, 363, 307 );

        // 4  PAYER'S/BORROWER'S name
        this.placeText( this.formatter.format( 'Name', this.doc.borrowerNameAddress ), 8, 379 );

        // 5  PAYER'S/BORROWER'S street address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.borrowerNameAddress ), 8, 451 );

        // 6  PAYER'S/BORROWER'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.borrowerNameAddress ), 8, 523 );

        // 7 9 Number of properties securing the mortgage
        this.placeTextCenter( this.formatter.format( 'integer', this.doc.mortgagedProperties ), 117, 595 );

        // 8 10 Other (property tax)
        this.placeTextRight( this.formatter.format5( 'LabeledDollar', 'Property tax', this.doc.propertyTax, 22, '' ), 486, 595 );

        // 9  Account number
        this.placeTextCenter( this.doc.accountNumber, 244, 667 );

        // 10 1 Mortgage interest received from borrower
        this.placeTextRight( this.formatter.format( 'number', this.doc.mortgageInterest ), 883, 211 );

        // 11 2 Outstanding mortgage principal
        this.placeTextRight( this.formatter.format( 'number', this.doc.outstandingPrincipal ), 686, 271 );

        // 12 3 Mortgage origination date
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.originationDate ), 786, 271 );

        // 13 4 Refund of overpaid interest
        this.placeTextRight( this.formatter.format( 'number', this.doc.overpaidRefund ), 686, 331 );

        // 14 5 Mortgage insurance premiums
        this.placeTextRight( this.formatter.format( 'number', this.doc.mortgageInsurance ), 883, 331 );

        // 15 6 Points paid on purchase of principal residence
        this.placeTextRight( this.formatter.format( 'number', this.doc.pointsPaid ), 883, 379 );

        // 16 7 Is address of property securing mortgage same as PAYER'S/BORROWER'S address
        this.placeText( this.formatter.format( 'Boolean', this.doc.isPropertyAddressSameAsBorrowerAddress ), 519, 400 );

        // 17 8 Address of property securing mortgage
        this.placeParagraph( this.formatter.format( 'Address', this.doc.propertyAddress ), 500, 515 );

        // 18 11 Mortgage acquisition date
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.acquisitionDate ), 967, 667 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}