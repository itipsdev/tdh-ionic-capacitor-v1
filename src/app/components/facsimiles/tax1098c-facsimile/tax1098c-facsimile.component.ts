import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098C } from '../../../models/tax1098C';


@Component({
    selector: 'app-tax1098c-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1098c.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1098CFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1077;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1077 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1098C;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 399, 15 );

        // 1  DONEE'S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.doneeNameAddress ), 6, 82 );

        // 2  DONEE'S TIN
        this.placeTextCenter( this.doc.doneeTin, 120, 306 );

        // 3  DONOR'S TIN
        this.placeTextCenter( this.doc.donorTin, 365, 306 );

        // 4  DONOR'S name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.donorNameAddress ), 6, 354 );

        // 5  DONOR'S Street address (including apt. no.)
        this.placeParagraph( this.formatter.format( 'StreetAddress', this.doc.donorNameAddress ), 6, 402 );

        // 6  DONOR'S City or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.donorNameAddress ), 6, 474 );

        // 7 1 Date of contribution
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.dateOfContribution ), 588, 90 );

        // 8 2a Odometer mileage
        this.placeTextCenter( this.formatter.format( 'integer', this.doc.odometerMileage ), 588, 162 );

        // 9 2b Year
        this.placeTextCenter( this.formatter.format( 'integer', this.doc.carYear ), 538, 210 );

        // 10 2c Make
        this.placeTextCenter( this.doc.make, 660, 210 );

        // 11 2d Model
        this.placeTextCenter( this.doc.model, 812, 210 );

        // 12 3 Vehicle or other identification number
        this.placeTextCenter( this.doc.vin, 689, 306 );

        // 13 4a Donee certifies that vehicle was sold in arm's length transaction to unrelated party
        this.placeText( this.formatter.format( 'Boolean', this.doc.armsLengthTransaction ), 529, 326 );

        // 14 4b Date of sale
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.dateOfSale ), 689, 426 );

        // 15 4c Gross proceeds from sale (see instructions)
        this.placeTextRight( this.formatter.format( 'number', this.doc.grossProceeds ), 889, 474 );

        // 16 5a Donee certifies that vehicle will not be transferred for money, other property, or services before completion of material improvements or significant intervening use
        this.placeText( this.formatter.format( 'Boolean', this.doc.notTransferredBefore ), 39, 494 );

        // 17 5b Donee certifies that vehicle is to be transferred to a needy individual for significantly below fair market value in furtherance of donee's charitable purpose
        this.placeText( this.formatter.format( 'Boolean', this.doc.needyIndividual ), 39, 566 );

        // 18 5c Donee certifies the following detailed description of material improvements or significant intervening use and duration of use
        this.placeParagraph( this.doc.descriptionOfImprovements, 6, 666 );

        // 19 6a Did you provide goods or services in exchange for the vehicle? Yes
        this.placeText( this.formatter.format( 'Boolean', this.doc.goodsInExchange ), 802, 782 );

        // 20 6a Did you provide goods or services in exchange for the vehicle? No
        this.placeText( '', 874, 782 );

        // 21 6b Value of goods and services provided in exchange for the vehicle
        this.placeText( this.formatter.format( 'number', this.doc.valueOfExchange ), 70, 858 );

        // 22 6c If this box is checked, donee certifies that the goods and services consisted solely of intangible religious benefits
        this.placeText( this.formatter.format( 'Boolean', this.doc.intangibleReligious ), 874, 890 );

        // 23 6c Describe the goods and services, if any, that were provided.
        this.placeParagraph( this.doc.descriptionOfGoods, 6, 930 );

        // 24 7 Under the law, the donor may not claim a deduction of more than $500 for this vehicle if this box is checked
        this.placeText( this.formatter.format( 'Boolean', this.doc.maxDeductionApplies ), 874, 1022 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}