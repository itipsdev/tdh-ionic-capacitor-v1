import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099K } from '../../../models/tax1099K';


@Component({
    selector: 'app-tax1099k-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099k.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099KFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 694;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 694 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099K;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 15 );

        // 1  FILER'S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.filerNameAddress ), 6, 91 );

        // 2  Check to indicate if FILER is a (an): Payment settlement entity (PSE)
        this.placeText( this.formatter.format( 'Boolean', this.doc.paymentSettlementEntity ), 223, 269 );

        // 3  Check to indicate if FILER is a (an): Electronic payment facilitator (EPF)/Other third party
        this.placeText( this.formatter.format( 'Boolean', this.doc.electronicPaymentFacilitator ), 223, 302 );

        // 4  Check to indicate transactions reported are: Payment card
        this.placeText( this.formatter.format( 'Boolean', this.doc.paymentCard ), 468, 269 );

        // 5  Check to indicate transactions reported are: Third party network
        this.placeText( this.formatter.format( 'Boolean', this.doc.thirdPartyNetwork ), 468, 302 );

        // 6  PAYEE'S name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.payeeNameAddress ), 6, 355 );

        // 7  PAYEE'S street address (including apt. no.)
        this.placeParagraph( this.formatter.format( 'StreetAddress', this.doc.payeeNameAddress ), 6, 427 );

        // 8  PAYEE'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.payeeNameAddress ), 6, 523 );

        // 9  PSE's name and telephone number
        this.placeParagraph( this.formatter.format4( 'NamePhone', this.doc.pseName, this.doc.psePhone, ' ' ), 6, 571 );

        // 10  Account number
        this.placeTextCenter( this.doc.accountNumber, 243, 667 );

        // 11  FILER'S TIN
        this.placeTextCenter( this.doc.filerTin, 588, 67 );

        // 12  PAYEE'S taxpayer identification no.
        this.placeTextCenter( this.doc.payeeFederalId, 588, 115 );

        // 13 1a Gross amount of payment card/third party network transactions
        this.placeTextRight( this.formatter.format( 'number', this.doc.grossAmount ), 687, 187 );

        // 14 1b Card Not Present Transactions
        this.placeTextRight( this.formatter.format( 'number', this.doc.cardNotPresent ), 687, 247 );

        // 15 2 Merchant category code
        this.placeTextCenter( this.doc.merchantCategoryCode, 790, 247 );

        // 16 3 Number of purchase transactions
        this.placeTextCenter( this.formatter.format( 'number', this.doc.numberOfTransactions ), 588, 307 );

        // 17 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 889, 307 );

        // 18 5a January
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'JAN' ), 687, 355 );

        // 19 5b February
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'FEB' ), 889, 355 );

        // 20 5c March
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'MAR' ), 687, 403 );

        // 21 5d April
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'APR' ), 889, 403 );

        // 22 5e May
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'MAY' ), 687, 451 );

        // 23 5f June
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'JUN' ), 889, 451 );

        // 24 5g July
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'JUL' ), 687, 499 );

        // 25 5h August
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'AUG' ), 889, 499 );

        // 26 5i September
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'SEP' ), 687, 547 );

        // 27 5j October
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'OCT' ), 889, 547 );

        // 28 5k November
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'NOV' ), 687, 595 );

        // 29 5l December
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.monthAmounts, 'DEC' ), 889, 595 );

        // 30 6 State
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 1 ), 589, 643 );

        // 31  State
        this.placeText( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 2 ), 495, 667 );

        // 32 7 State identification number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 1 ), 790, 643 );

        // 33  State identification number
        this.placeText( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 2 ), 695, 667 );

        // 34 8 State income tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 1049, 643 );

        // 35  State income tax withheld
        this.placeText( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 2 ), 917, 667 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}