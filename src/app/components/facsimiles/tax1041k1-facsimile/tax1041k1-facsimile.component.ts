import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1041K1 } from '../../../models/tax1041K1';


@Component({
    selector: 'app-tax1041k1-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1041sk1.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1041K1FacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1439;

    trimWidth: number = 1082;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1439 / 1082;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1041K1;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1082;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  Final K-1
        this.placeText( this.formatter.format( 'Boolean', this.doc.finalK1 ), 580, 41 );

        // 1  Amended K-1
        this.placeText( this.formatter.format( 'Boolean', this.doc.amendedK1 ), 753, 41 );

        // 2  Tax year beginning month
        this.placeText( '', 148, 163 );

        // 3  Tax year beginning day
        this.placeText( '', 197, 163 );

        // 4  Tax year beginning year
        this.placeText( '', 240, 163 );

        // 5  Tax year ending month
        this.placeText( '', 393, 163 );

        // 6  Tax year ending day
        this.placeText( '', 442, 163 );

        // 7  Tax year ending year
        this.placeText( '', 485, 163 );

        // 8 A Estate's or trust's employer identification number
        this.placeTextCenter( this.doc.trustTin, 280, 331 );

        // 9 B Estate's or trust's name
        this.placeText( this.doc.trustName, 7, 451 );

        // 10 C Fiduciary's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.fiduciaryNameAddress ), 7, 495 );

        // 11 D Check if Form 1041-T was filed
        this.placeText( this.formatter.format( 'Boolean', this.doc.final1041T ), 33, 688 );

        // 12 D and enter the date it was filed
        this.placeText( this.formatter.format( 'Timestamp', this.doc.date1041T ), 77, 715 );

        // 13 E Check if this is the final Form 1041 for the estate or trust
        this.placeText( this.formatter.format( 'Boolean', this.doc.final1041 ), 35, 760 );

        // 14 F Beneficiary's identifying number
        this.placeText( this.doc.beneficiaryTin, 5, 859 );

        // 15 G Beneficiary's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.beneficiaryNameAddress ), 7, 903 );

        // 16 H Domestic beneficiary
        this.placeText( this.formatter.format( 'Boolean', this.doc.domestic ), 34, 1400 );

        // 17 H Foreign beneficiary
        this.placeText( this.formatter.format( 'Boolean', this.doc.foreign ), 293, 1400 );

        // 18 1 Interest income
        this.placeTextRight( this.formatter.format( 'number', this.doc.interestIncome ), 818, 139 );

        // 19 2a Ordinary dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryDividends ), 818, 187 );

        // 20 2b Qualified dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedDividends ), 818, 235 );

        // 21 3 Net short-term capital gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.netShortTermGain ), 818, 283 );

        // 22 4a Net long-term capital gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.netLongTermGain ), 818, 331 );

        // 23 4b 28% rate gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.gain28Rate ), 818, 379 );

        // 24 4c Unrecaptured section 1250 gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.unrecaptured1250Gain ), 818, 427 );

        // 25 5 Other portfolio and nonbusiness income
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherPortfolioIncome ), 818, 499 );

        // 26 6 Ordinary business income
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryBusinessIncome ), 818, 547 );

        // 27 7 Net rental real estate income
        this.placeTextRight( this.formatter.format( 'number', this.doc.netRentalRealEstateIncome ), 818, 595 );

        // 28 8 Other rental income
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherRentalIncome ), 818, 643 );

        // 29 9 Directly apportioned deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.directlyApportionedDeductions, 1 ), 567, 691 );

        // 30 9 Directly apportioned deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.directlyApportionedDeductions, 1 ), 818, 691 );

        // 31 9 Directly apportioned deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.directlyApportionedDeductions, 2 ), 567, 739 );

        // 32 9 Directly apportioned deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.directlyApportionedDeductions, 2 ), 818, 739 );

        // 33 9 Directly apportioned deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.directlyApportionedDeductions, 3 ), 567, 787 );

        // 34 9 Directly apportioned deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.directlyApportionedDeductions, 3 ), 818, 787 );

        // 35 10 Estate tax deduction
        this.placeTextRight( this.formatter.format( 'number', this.doc.estateTaxDeduction ), 818, 835 );

        // 36 11 Final year deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.finalYearDeductions, 1 ), 826, 139 );

        // 37 11 Final year deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.finalYearDeductions, 1 ), 1078, 139 );

        // 38 11 Final year deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.finalYearDeductions, 2 ), 826, 187 );

        // 39 11 Final year deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.finalYearDeductions, 2 ), 1078, 187 );

        // 40 11 Final year deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.finalYearDeductions, 3 ), 826, 235 );

        // 41 11 Final year deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.finalYearDeductions, 3 ), 1078, 235 );

        // 42 11 Final year deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.finalYearDeductions, 4 ), 826, 283 );

        // 43 11 Final year deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.finalYearDeductions, 4 ), 1078, 283 );

        // 44 11 Final year deductions
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.finalYearDeductions, 5 ), 826, 331 );

        // 45 11 Final year deductions
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.finalYearDeductions, 5 ), 1078, 331 );

        // 46 12 Alternative minimum tax adjustment
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.amtAdjustments, 1 ), 826, 379 );

        // 47 12 Alternative minimum tax adjustment
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtAdjustments, 1 ), 1078, 379 );

        // 48 12 Alternative minimum tax adjustment
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.amtAdjustments, 2 ), 826, 427 );

        // 49 12 Alternative minimum tax adjustment
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtAdjustments, 2 ), 1078, 427 );

        // 50 12 Alternative minimum tax adjustment
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.amtAdjustments, 3 ), 826, 475 );

        // 51 12 Alternative minimum tax adjustment
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtAdjustments, 3 ), 1078, 475 );

        // 52 12 Alternative minimum tax adjustment
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.amtAdjustments, 4 ), 826, 523 );

        // 53 12 Alternative minimum tax adjustment
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtAdjustments, 4 ), 1078, 523 );

        // 54 12 Alternative minimum tax adjustment
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.amtAdjustments, 5 ), 826, 571 );

        // 55 12 Alternative minimum tax adjustment
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.amtAdjustments, 5 ), 1078, 571 );

        // 56 13 Credits and credit recapture
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.credits, 1 ), 826, 619 );

        // 57 13 Credits and credit recapture
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 1 ), 1078, 619 );

        // 58 13 Credits and credit recapture
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.credits, 2 ), 826, 667 );

        // 59 13 Credits and credit recapture
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 2 ), 1078, 667 );

        // 60 13 Credits and credit recapture
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.credits, 3 ), 826, 715 );

        // 61 13 Credits and credit recapture
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.credits, 3 ), 1078, 715 );

        // 62 14 Other information
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 1 ), 826, 763 );

        // 63 14 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 1 ), 1078, 763 );

        // 64 14 Other information
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 2 ), 826, 811 );

        // 65 14 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 2 ), 1078, 811 );

        // 66 14 Other information
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 3 ), 826, 859 );

        // 67 14 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 3 ), 1078, 859 );

        // 68 14 Other information
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 4 ), 826, 907 );

        // 69 14 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 4 ), 1078, 907 );

        // 70 14 Other information
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 5 ), 826, 955 );

        // 71 14 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 5 ), 1078, 955 );

        // 72 14 Other information
        this.placeText( this.formatter.format3( 'CodeLetter', this.doc.otherInfo, 6 ), 826, 1003 );

        // 73 14 Other information
        this.placeTextRight( this.formatter.format3( 'CodeAmount', this.doc.otherInfo, 6 ), 1078, 1003 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}