import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1095B } from '../../../models/tax1095B';


@Component({
    selector: 'app-tax1095b-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1095b.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1095BFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 1102;

    trimWidth: number = 1443;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 1102 / 1443;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1095B;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1443;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0   VOID
        this.placeText( '', 1055, 66 );

        // 1   CORRECTED
        this.placeText( '', 1055, 115 );

        // 2 1 Name of responsible individual - first name
        this.placeText( 'combo-x' /* getFirstName( responsibleName ) */, 4, 209 );

        // 3  Name of responsible individual - middle name
        this.placeText( 'combo-x' /* getMiddleInitial( responsibleName ) */, 265, 209 );

        // 4  Name of responsible individual - last name
        this.placeText( 'combo-x' /* getLastName( responsibleName ) */, 524, 209 );

        // 5 2 Social security number (SSN or other TIN)
        this.placeText( this.doc.responsibleTin, 782, 209 );

        // 6 3 Date of birth (if SSN or other TIN is not available)
        this.placeText( this.formatter.format( 'Timestamp', this.doc.responsibleDateOfBirth ), 1084, 209 );

        // 7 4 Street address (including apartment no.)
        this.placeText( 'combo-x' /* getStreetAddress( responsibleAddress ) */, 4, 257 );

        // 8 5 City or town
        this.placeText( 'combo-x' /* getCity( responsibleAddress ) */, 510, 257 );

        // 9 6 State or province
        this.placeText( 'combo-x' /* getState( responsibleAddress ) */, 784, 257 );

        // 10 7 Country and ZIP or foreign postal code
        this.placeText( 'combo-x' /* getZip( responsibleAddress ) */, 1086, 257 );

        // 11 8 Enter letter identifying Origin of the Health Coverage (see instructions for codes):
        this.placeText( 'combo-x' /* text( data.getOriginOfHealthCoverageCode( ) ) */, 738, 303 );

        // 12 9 Reserved
        this.placeText( '', 784, 305 );

        // 13 10 Employer name
        this.placeText( 'combo-x' /* getName( employerNameAddress ) */, 4, 377 );

        // 14 11 Employer identification number (EIN)
        this.placeText( this.doc.employerId, 1084, 377 );

        // 15 12 Street address (including room or suite no.)
        this.placeText( 'combo-x' /* getStreetAddress( employerNameAddress ) */, 4, 425 );

        // 16 13 City or town
        this.placeText( 'combo-x' /* getCity( employerNameAddress ) */, 510, 425 );

        // 17 14 State or province
        this.placeText( 'combo-x' /* getState( employerNameAddress ) */, 784, 425 );

        // 18 15 Country and ZIP or foreign postal code
        this.placeText( 'combo-x' /* getZip( employerNameAddress ) */, 1086, 425 );

        // 19 16 Name
        this.placeText( 'combo-x' /* getName( issuerNameAddressPhone ) */, 4, 497 );

        // 20 17 Employer identification number (EIN)
        this.placeText( this.doc.issuerId, 782, 497 );

        // 21 18 Contact telephone number
        this.placeText( 'combo-x' /* getPhone( issuerNameAddressPhone ) */, 1084, 497 );

        // 22 19 Street address (including room or suite no.)
        this.placeText( 'combo-x' /* getStreetAddress( issuerNameAddressPhone ) */, 4, 545 );

        // 23 20 City or town
        this.placeText( 'combo-x' /* getCity( issuerNameAddressPhone ) */, 510, 545 );

        // 24 21 State or province
        this.placeText( 'combo-x' /* getState( issuerNameAddressPhone ) */, 784, 545 );

        // 25 22 Country and ZIP or foreign postal code
        this.placeText( 'combo-x' /* getZip( issuerNameAddressPhone ) */, 1086, 545 );

        // 26 23a Name of covered individual(s) - first name
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 1 ) */, 33, 713 );

        // 27 23a Name of covered individual(s) - middle initial
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 1 ) */, 177, 713 );

        // 28 23a Name of covered individual(s) - last name
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 1 ) */, 222, 713 );

        // 29 23b SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 1 ) */, 364, 713 );

        // 30 23c DOB (If SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 1 ) */, 537, 713 );

        // 31 23d Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 1 ) */, 704, 691 );

        // 32 23e Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 1 ) */, 769, 691 );

        // 33  Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 2 ) */, 827, 691 );

        // 34  Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 3 ) */, 884, 691 );

        // 35  Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 4 ) */, 942, 691 );

        // 36  May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 5 ) */, 1000, 691 );

        // 37  Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 6 ) */, 1057, 691 );

        // 38  Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 7 ) */, 1115, 691 );

        // 39  Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 8 ) */, 1172, 691 );

        // 40  Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 9 ) */, 1230, 691 );

        // 41  Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 10 ) */, 1288, 691 );

        // 42  Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 11 ) */, 1345, 691 );

        // 43  Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 1, 12 ) */, 1403, 691 );

        // 44 24a Name of covered individual(s) - first name
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 2 ) */, 33, 785 );

        // 45 23a Name of covered individual(s) - middle initial
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 2 ) */, 177, 785 );

        // 46 23a Name of covered individual(s) - last name
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 2 ) */, 222, 785 );

        // 47 23b SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 2 ) */, 364, 785 );

        // 48 23c DOB (If SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 2 ) */, 537, 785 );

        // 49 23d Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 2 ) */, 704, 763 );

        // 50 23e Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 1 ) */, 769, 763 );

        // 51  Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 2 ) */, 827, 763 );

        // 52  Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 3 ) */, 884, 763 );

        // 53  Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 4 ) */, 942, 763 );

        // 54  May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 5 ) */, 1000, 763 );

        // 55  Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 6 ) */, 1057, 763 );

        // 56  Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 7 ) */, 1115, 763 );

        // 57  Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 8 ) */, 1172, 763 );

        // 58  Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 9 ) */, 1230, 763 );

        // 59  Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 10 ) */, 1288, 763 );

        // 60  Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 11 ) */, 1345, 763 );

        // 61  Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 2, 12 ) */, 1403, 763 );

        // 62 25a Name of covered individual(s) - first name
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 3 ) */, 33, 857 );

        // 63 23a Name of covered individual(s) - middle initial
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 3 ) */, 177, 857 );

        // 64 23a Name of covered individual(s) - last name
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 3 ) */, 222, 857 );

        // 65 23b SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 3 ) */, 364, 857 );

        // 66 23c DOB (If SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 3 ) */, 537, 857 );

        // 67 23d Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 3 ) */, 704, 835 );

        // 68 23e Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 1 ) */, 769, 835 );

        // 69  Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 2 ) */, 827, 835 );

        // 70  Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 3 ) */, 884, 835 );

        // 71  Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 4 ) */, 942, 835 );

        // 72  May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 5 ) */, 1000, 835 );

        // 73  Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 6 ) */, 1057, 835 );

        // 74  Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 7 ) */, 1115, 835 );

        // 75  Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 8 ) */, 1172, 835 );

        // 76  Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 9 ) */, 1230, 835 );

        // 77  Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 10 ) */, 1288, 835 );

        // 78  Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 11 ) */, 1345, 835 );

        // 79  Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 3, 12 ) */, 1403, 835 );

        // 80 26a Name of covered individual(s) - first name
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 4 ) */, 33, 929 );

        // 81 23a Name of covered individual(s) - middle initial
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 4 ) */, 177, 929 );

        // 82 23a Name of covered individual(s) - last name
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 4 ) */, 222, 929 );

        // 83 23b SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 4 ) */, 364, 929 );

        // 84 23c DOB (If SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 4 ) */, 537, 929 );

        // 85 23d Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 1 ) */, 704, 907 );

        // 86 23e Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 1 ) */, 769, 907 );

        // 87  Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 2 ) */, 827, 907 );

        // 88  Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 3 ) */, 884, 907 );

        // 89  Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 4 ) */, 942, 907 );

        // 90  May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 5 ) */, 1000, 907 );

        // 91  Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 6 ) */, 1057, 907 );

        // 92  Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 7 ) */, 1115, 907 );

        // 93  Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 8 ) */, 1172, 907 );

        // 94  Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 9 ) */, 1230, 907 );

        // 95  Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 10 ) */, 1288, 907 );

        // 96  Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 11 ) */, 1345, 907 );

        // 97  Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 4, 12 ) */, 1403, 907 );

        // 98 27a Name of covered individual(s) - first name
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 5 ) */, 33, 1001 );

        // 99 23a Name of covered individual(s) - middle initial
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 5 ) */, 177, 1001 );

        // 100 23a Name of covered individual(s) - last name
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 5 ) */, 222, 1001 );

        // 101 23b SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 5 ) */, 364, 1001 );

        // 102 23c DOB (If SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 5 ) */, 537, 1001 );

        // 103 23d Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 1 ) */, 704, 979 );

        // 104 23e Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 1 ) */, 769, 979 );

        // 105  Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 2 ) */, 827, 979 );

        // 106  Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 3 ) */, 884, 979 );

        // 107  Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 4 ) */, 942, 979 );

        // 108  May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 5 ) */, 1000, 979 );

        // 109  Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 6 ) */, 1057, 979 );

        // 110  Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 7 ) */, 1115, 979 );

        // 111  Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 8 ) */, 1172, 979 );

        // 112  Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 9 ) */, 1230, 979 );

        // 113  Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 10 ) */, 1288, 979 );

        // 114  Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 11 ) */, 1345, 979 );

        // 115  Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 5, 12 ) */, 1403, 979 );

        // 116 28a Name of covered individual(s) - first name
        this.placeText( 'combo-x' /* getCoveredIndividualFirstName( coveredIndividuals, 6 ) */, 33, 1073 );

        // 117 23a Name of covered individual(s) - middle initial
        this.placeText( 'combo-x' /* getCoveredIndividualMiddleInitial( coveredIndividuals, 6 ) */, 177, 1073 );

        // 118 23a Name of covered individual(s) - last name
        this.placeText( 'combo-x' /* getCoveredIndividualLastName( coveredIndividuals, 6 ) */, 222, 1073 );

        // 119 23b SSN or other TIN
        this.placeText( 'combo-x' /* getCoveredIndividualTin( coveredIndividuals, 6 ) */, 364, 1073 );

        // 120 23c DOB (If SSN or other TIN is not available)
        this.placeText( 'combo-x' /* getCoveredIndividualDob( coveredIndividuals, 6 ) */, 537, 1073 );

        // 121 23d Covered all 12 months
        this.placeText( 'combo-x' /* getCoveredIndividualCovered12( coveredIndividuals, 1 ) */, 704, 1051 );

        // 122 23e Jan
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 1 ) */, 769, 1051 );

        // 123  Feb
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 2 ) */, 827, 1051 );

        // 124  Mar
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 3 ) */, 884, 1051 );

        // 125  Apr
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 4 ) */, 942, 1051 );

        // 126  May
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 5 ) */, 1000, 1051 );

        // 127  Jun
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 6 ) */, 1057, 1051 );

        // 128  Jul
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 7 ) */, 1115, 1051 );

        // 129  Aug
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 8 ) */, 1172, 1051 );

        // 130  Sep
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 9 ) */, 1230, 1051 );

        // 131  Oct
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 10 ) */, 1288, 1051 );

        // 132  Nov
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 11 ) */, 1345, 1051 );

        // 133  Dec
        this.placeText( 'combo-x' /* getCoveredIndividualCoveredMonth( coveredIndividuals, 6, 12 ) */, 1403, 1051 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}