import { AfterViewInit, Component, OnInit } from '@angular/core';

import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner/ngx';

import { GlobalDataService } from '../../services/global-data.service';

import { NavigationExtras, Router } from '@angular/router';
import {LoggerService} from '../../services/logger.service';

@Component( {
    selector: 'app-scan-doc',
    templateUrl: './scan-doc.component.html',
    styleUrls: [ './scan-doc.component.scss' ],
} )
export class ScanDocComponent implements OnInit, AfterViewInit {

    options: BarcodeScannerOptions;

    results: BarcodeScanResult;

    constructor(
        private scanner: BarcodeScanner,
        private router: Router,
        private logger: LoggerService,
        private global: GlobalDataService
    ) {
    }

    ngOnInit( ) {

        this.logger.trace( 'ScanDocComponent ngOnInit ' );

        // this.scanQrCode( );

    }

    ngAfterViewInit() {

        this.logger.trace( 'ScanDocComponent ngAfterViewInit');

        // this.scanQrCode( );

    }

    scanQrCode( ) {

        this.logger.trace( 'ScanDocComponent scanQrCode' )

        // Options
        // https://ionicframework.com/docs/v3/native/barcode-scanner/

        this.options = {

            preferFrontCamera: false,

            showFlipCameraButton: false,

            showTorchButton: true,

            // formats. string. Defaults to all formats except PDF_417 and RSS_EXPANDED.
            // 'QR_CODE' | 'DATA_MATRIX' | 'UPC_E' | 'UPC_A' | 'EAN_8' | 'EAN_13' | 'CODE_128'
            // 'CODE_39' | 'CODE_93' | 'CODABAR' | 'ITF'
            // 'RSS14' | 'RSS_EXPANDED' | 'PDF_417' | 'AZTEC' | 'MSI'
            formats: 'QR_CODE',


            // =====================================================
            // Android only
            // =====================================================
            prompt: 'Scan QR code',

            // orientation. string. Can be set to portrait or landscape.
            // Defaults to none so the user can rotate the phone and pick an orientation
            // orientation: ''

            // torchOn	boolean	Launch with the torch switched on (if available).
            torchOn: true,

            // resultDisplayDuration  number
            // Display scanned text for X ms. 0 suppresses it entirely, default 1500.
            resultDisplayDuration: 0,


            // =====================================================
            // iOS only
            // =====================================================

            disableAnimations: false,

            disableSuccessBeep: false,

        };

        this.global.qrData = '';

        this.global.serverResponse = '';

        this.scanner
            .scan( this.options )
            .then(
                ( barCodeData ) => {

                    if ( barCodeData.cancelled ) {

                        this.goToWelcome( );

                    } else {

                        this.logger.trace( 'scanQrCode completed' );

                        this.logger.debugWithLabel( barCodeData.format, 'barcode format' );

                        const docId: number = this.global.recordScannedText(
                            barCodeData.text
                        );

                        this.logger.debugWithLabel( '' + docId, 'docId' );

                        if ( docId > -1 ) {

                            this.goToSuccessScreen( docId );

                        } else {

                            this.goToFailureScreen( docId );

                        }

                    }

                },
                ( error ) => {

                    this.logger.dump( 'scanner error', error );

                    this.global.lastScannedText = JSON.stringify( error, null, 4 );

                    this.goToFailureScreen( this.global.SCAN_ERROR );

                }
            );

    }

    goToSuccessScreen(
        docId: number
    ) {

        this.logger.trace( `ScanDocComponent goToSuccessScreen ${docId}` );

        this.router.navigate(
            [ '/', 'doc-facsimile', docId ]
        );

        // this.router.navigate(
        //     [ '/', 'docs' ]
        // );

    }

    goToFailureScreen(
        errorCode: number
    ) {

        this.logger.trace( 'ScanDocComponent goToFailureScreen' );

        const navigationExtras: NavigationExtras = {
            queryParams: { 'errorCode': errorCode }
        };

        this.router.navigate(
            [ '/', 'scan-error' ],
            navigationExtras
        );

    }

    goToWelcome( ) {

        this.logger.trace( 'ScanDocComponent goToWelcome' );

        this.router.navigate( [ '/' ] );

    }

}
