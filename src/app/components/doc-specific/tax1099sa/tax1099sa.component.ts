import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Sa } from '../../../models/tax1099Sa';


@Component({
    selector: 'app-tax1099sa',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossDistribution">
    <ion-label>Gross distribution</ion-label>
    <ion-chip color="primary">{{ doc.grossDistribution }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.earnings">
    <ion-label>Earnings on excess cont.</ion-label>
    <ion-chip color="primary">{{ doc.earnings }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.distributionCode">
    <ion-label>Distribution code</ion-label>
    <ion-chip color="primary">{{ doc.distributionCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fairMarketValue">
    <ion-label>FMV on date of death</ion-label>
    <ion-chip color="primary">{{ doc.fairMarketValue }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.hsa">
    <ion-label>HSA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.hsa ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.archerAccount">
    <ion-label>Archer MSA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.archerAccount ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.medicalSavingsAccount">
    <ion-label>MA MSA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.medicalSavingsAccount ) }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099SaComponent implements OnInit {

    @Input()
    public doc: Tax1099Sa;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
