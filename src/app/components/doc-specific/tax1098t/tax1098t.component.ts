import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098T } from '../../../models/tax1098T';


@Component({
    selector: 'app-tax1098t',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.filerNameAddress">
    <ion-label>Filer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.filerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.filerTin">
    <ion-label>Filer's federal identification number</ion-label>
    <ion-chip color="primary">{{ doc.filerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.studentTin">
    <ion-label>Student's social security number</ion-label>
    <ion-chip color="primary">{{ doc.studentTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.studentNameAddress">
    <ion-label>Student's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.studentNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.halfTime">
    <ion-label>Check if at least half-time student</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.halfTime ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedTuitionFees">
    <ion-label>Payments received for qualified tuition and related expenses</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedTuitionFees }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.adjustmentPriorYear">
    <ion-label>Adjustments made for a prior year</ion-label>
    <ion-chip color="primary">{{ doc.adjustmentPriorYear }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.scholarship">
    <ion-label>Scholarships or grants</ion-label>
    <ion-chip color="primary">{{ doc.scholarship }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.adjustScholarship">
    <ion-label>Adjustments to scholarships or grants for a prior year</ion-label>
    <ion-chip color="primary">{{ doc.adjustScholarship }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.includeJanMar">
    <ion-label>Check if the amount in box 1 or box 2 includes amounts for an academic period beginning January - March 2018</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.includeJanMar ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.graduate">
    <ion-label>Check if graduate student</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.graduate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.insuranceRefund">
    <ion-label>Insurance contract reimbursement / refund</ion-label>
    <ion-chip color="primary">{{ doc.insuranceRefund }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1098TComponent implements OnInit {

    @Input()
    public doc: Tax1098T;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
