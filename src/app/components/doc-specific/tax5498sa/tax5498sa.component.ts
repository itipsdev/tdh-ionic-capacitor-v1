import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax5498Sa } from '../../../models/tax5498Sa';


@Component({
    selector: 'app-tax5498sa',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.trusteeNameAddress">
    <ion-label>Trustee's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.trusteeNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.trusteeTin">
    <ion-label>TRUSTEE'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.trusteeTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.participantTin">
    <ion-label>PARTICIPANT'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.participantTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.participantNameAddress">
    <ion-label>Participant's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.participantNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.msaContributions">
    <ion-label>Employee or self-employed person's Archer MSA contributions made in 2019 and 2020 for 2019</ion-label>
    <ion-chip color="primary">{{ doc.msaContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.totalContributions">
    <ion-label>Total contributions made in 2019</ion-label>
    <ion-chip color="primary">{{ doc.totalContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.totalPostYearEnd">
    <ion-label>Total HSA or Archer MSA contributions made in 2020 for 2019</ion-label>
    <ion-chip color="primary">{{ doc.totalPostYearEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rolloverContributions">
    <ion-label>Rollover contributions</ion-label>
    <ion-chip color="primary">{{ doc.rolloverContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fairMarketValue">
    <ion-label>Fair market value OF HAS, Archer MSA, or MA MSA</ion-label>
    <ion-chip color="primary">{{ doc.fairMarketValue }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.hsa">
    <ion-label>HSA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.hsa ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.archer">
    <ion-label>Archer MSA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.archer ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.maMsa">
    <ion-label>MA MSA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.maMsa ) }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax5498SaComponent implements OnInit {

    @Input()
    public doc: Tax5498Sa;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
