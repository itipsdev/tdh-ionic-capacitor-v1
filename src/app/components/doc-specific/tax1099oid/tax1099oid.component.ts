import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Oid } from '../../../models/tax1099Oid';


@Component({
    selector: 'app-tax1099oid',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>Payer's TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>Recipient's TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.foreignAccountTaxCompliance">
    <ion-label>FATCA filing requirement</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreignAccountTaxCompliance ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.originalIssueDiscount">
    <ion-label>Original issue discount</ion-label>
    <ion-chip color="primary">{{ doc.originalIssueDiscount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherPeriodicInterest">
    <ion-label>Other periodic interest</ion-label>
    <ion-chip color="primary">{{ doc.otherPeriodicInterest }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.earlyWithdrawalPenalty">
    <ion-label>Early withdrawal penalty</ion-label>
    <ion-chip color="primary">{{ doc.earlyWithdrawalPenalty }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.marketDiscount">
    <ion-label>Market discount</ion-label>
    <ion-chip color="primary">{{ doc.marketDiscount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.acquisitionPremium">
    <ion-label>Acquisition premium</ion-label>
    <ion-chip color="primary">{{ doc.acquisitionPremium }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.description">
    <ion-label>Description</ion-label>
    <ion-chip color="primary">{{ doc.description }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.discountOnTreasuryObligations">
    <ion-label>Original issue discount on U.S. Treasury obligations</ion-label>
    <ion-chip color="primary">{{ doc.discountOnTreasuryObligations }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.investmentExpenses">
    <ion-label>Investment expenses</ion-label>
    <ion-chip color="primary">{{ doc.investmentExpenses }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bondPremium">
    <ion-label>Bond premium</ion-label>
    <ion-chip color="primary">{{ doc.bondPremium }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxExemptOid">
    <ion-label>Tax-exempt OID</ion-label>
    <ion-chip color="primary">{{ doc.taxExemptOid }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withheld</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099OidComponent implements OnInit {

    @Input()
    public doc: Tax1099Oid;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
