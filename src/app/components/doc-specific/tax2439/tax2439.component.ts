import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax2439 } from '../../../models/tax2439';


@Component({
    selector: 'app-tax2439',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.fiscalYearBegin">
    <ion-label>Fiscal year begin date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearBegin ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fiscalYearEnd">
    <ion-label>Fiscal year end date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearEnd ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ricOrReitNameAddress">
    <ion-label>RIC or REIT's address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.ricOrReitNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.ricOrReitTin">
    <ion-label>Identification number of RIC or REIT</ion-label>
    <ion-chip color="primary">{{ doc.ricOrReitTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.shareholderNameAddress">
    <ion-label>Shareholder's address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.shareholderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.shareholderTin">
    <ion-label>Shareholder's identifying number</ion-label>
    <ion-chip color="primary">{{ doc.shareholderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.undistributedLongTermCapitalGains">
    <ion-label>Total undistributed long-term capital gains</ion-label>
    <ion-chip color="primary">{{ doc.undistributedLongTermCapitalGains }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrecaptured1250Gain">
    <ion-label>Unrecaptured section 1250 gain</ion-label>
    <ion-chip color="primary">{{ doc.unrecaptured1250Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.section1202Gain">
    <ion-label>Section 1202 gain</ion-label>
    <ion-chip color="primary">{{ doc.section1202Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.collectiblesGain">
    <ion-label>Collectibles (28%) gain</ion-label>
    <ion-chip color="primary">{{ doc.collectiblesGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxPaid">
    <ion-label>Tax paid by the RIC or REIT on the box 1a gains</ion-label>
    <ion-chip color="primary">{{ doc.taxPaid }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax2439Component implements OnInit {

    @Input()
    public doc: Tax2439;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
