import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1065K1 } from '../../../models/tax1065K1';


@Component({
    selector: 'app-tax1065k1',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.fiscalYearBegin">
    <ion-label>Fiscal year begin date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearBegin ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fiscalYearEnd">
    <ion-label>Fiscal year end data</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearEnd ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.finalK1">
    <ion-label>Final K-1</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.finalK1 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.amendedK1">
    <ion-label>Amended K-1</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.amendedK1 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.partnershipTin">
    <ion-label>Partnership's employer identification number</ion-label>
    <ion-chip color="primary">{{ doc.partnershipTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.partnershipNameAddress">
    <ion-label>Partnership's name, address, city, state, and ZIP code</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.partnershipNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.irsCenter">
    <ion-label>IRS Center where partnership filed return</ion-label>
    <ion-chip color="primary">{{ doc.irsCenter }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.publiclyTraded">
    <ion-label>Check if this is a publicly traded partnership (PTP)</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.publiclyTraded ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.partnerTin">
    <ion-label>Partner's identifying number</ion-label>
    <ion-chip color="primary">{{ doc.partnerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.partnerNameAddress">
    <ion-label>Partner's name, address, city, state, and ZIP code</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.partnerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.generalPartner">
    <ion-label>General partner or LLC member-manager</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.generalPartner ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.limitedPartner">
    <ion-label>Limited partner or other LLC member</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.limitedPartner ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.domestic">
    <ion-label>Domestic partner</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.domestic ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreign">
    <ion-label>Foreign partner</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreign ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.entityType">
    <ion-label>What type of entity is this partner?</ion-label>
    <ion-chip color="primary">{{ doc.entityType }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.retirementPlan">
    <ion-label>If this partner is a retirement plan (IRA/SEP/Keogh/etc.), check here</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.retirementPlan ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.profitShareBegin">
    <ion-label>Partner's share of profit - beginning</ion-label>
    <ion-chip color="primary">{{ doc.profitShareBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.profitShareEnd">
    <ion-label>Partner's share of profit - ending</ion-label>
    <ion-chip color="primary">{{ doc.profitShareEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.lossShareBegin">
    <ion-label>Partner's share of loss - beginning</ion-label>
    <ion-chip color="primary">{{ doc.lossShareBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.lossShareEnd">
    <ion-label>Partner's share of loss - ending</ion-label>
    <ion-chip color="primary">{{ doc.lossShareEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalShareBegin">
    <ion-label>Partner's share of capital - beginning</ion-label>
    <ion-chip color="primary">{{ doc.capitalShareBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalShareEnd">
    <ion-label>Partner's share of capital - ending</ion-label>
    <ion-chip color="primary">{{ doc.capitalShareEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonrecourseLiabilityShareBegin">
    <ion-label>Partner's share of liabilities - beginning - nonrecourse</ion-label>
    <ion-chip color="primary">{{ doc.nonrecourseLiabilityShareBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonrecourseLiabilityShareEnd">
    <ion-label>Partner's share of liabilities - ending - nonrecourse</ion-label>
    <ion-chip color="primary">{{ doc.nonrecourseLiabilityShareEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedLiabilityShareBegin">
    <ion-label>Partner's share of liabilities - beginning - qualified nonrecourse financing</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedLiabilityShareBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedLiabilityShareEnd">
    <ion-label>Partner's share of liabilities - ending - qualified nonrecourse financing</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedLiabilityShareEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recourseLiabilityShareBegin">
    <ion-label>Partner's share of liabilities - beginning - recourse</ion-label>
    <ion-chip color="primary">{{ doc.recourseLiabilityShareBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recourseLiabilityShareEnd">
    <ion-label>Partner's share of liabilities - ending - recourse</ion-label>
    <ion-chip color="primary">{{ doc.recourseLiabilityShareEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalAccountBegin">
    <ion-label>Partner's capital account analysis - Beginning capital account</ion-label>
    <ion-chip color="primary">{{ doc.capitalAccountBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalAccountContributions">
    <ion-label>Partner's capital account analysis - Capital contributed during the year</ion-label>
    <ion-chip color="primary">{{ doc.capitalAccountContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalAccountIncrease">
    <ion-label>Partner's capital account analysis - Current year increase (decrease)</ion-label>
    <ion-chip color="primary">{{ doc.capitalAccountIncrease }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalAccountWithdrawals">
    <ion-label>Partner's capital account analysis - Withdrawals & distributions</ion-label>
    <ion-chip color="primary">{{ doc.capitalAccountWithdrawals }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalAccountEnd">
    <ion-label>Partner's capital account analysis - Ending capital account</ion-label>
    <ion-chip color="primary">{{ doc.capitalAccountEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bookTax">
    <ion-label>Tax basis</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.bookTax ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bookGaap">
    <ion-label>GAAP</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.bookGaap ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.book704b">
    <ion-label>Section 704(b) book</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.book704b ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bookOther">
    <ion-label>Other (explain)</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.bookOther ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bookOtherExplain">
    <ion-label>Other (explain)</ion-label>
    <ion-chip color="primary">{{ doc.bookOtherExplain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.builtInGain">
    <ion-label>Did the partner contribute property with a built-in gain or loss? - Yes</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.builtInGain ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryIncome">
    <ion-label>Ordinary business income (loss)</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netRentalRealEstateIncome">
    <ion-label>Net rental real estate income (loss)</ion-label>
    <ion-chip color="primary">{{ doc.netRentalRealEstateIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherRentalIncome">
    <ion-label>Other net rental income (loss)</ion-label>
    <ion-chip color="primary">{{ doc.otherRentalIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.guaranteedPayment">
    <ion-label>Guaranteed payments</ion-label>
    <ion-chip color="primary">{{ doc.guaranteedPayment }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.interestIncome">
    <ion-label>Interest income</ion-label>
    <ion-chip color="primary">{{ doc.interestIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryDividends">
    <ion-label>Ordinary dividends</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedDividends">
    <ion-label>Qualified dividends</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dividendEquivalents">
    <ion-label>Dividend equivalents</ion-label>
    <ion-chip color="primary">{{ doc.dividendEquivalents }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.royalties">
    <ion-label>Royalties</ion-label>
    <ion-chip color="primary">{{ doc.royalties }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netShortTermGain">
    <ion-label>Net short-term capital gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.netShortTermGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netLongTermGain">
    <ion-label>Net long-term capital gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.netLongTermGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.collectiblesGain">
    <ion-label>Collectibles (28%) gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.collectiblesGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrecaptured1250Gain">
    <ion-label>Unrecaptured section 1250 gain</ion-label>
    <ion-chip color="primary">{{ doc.unrecaptured1250Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.net1231Gain">
    <ion-label>Net section 1231 gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.net1231Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherIncome">
    <ion-label>Other income</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherIncome">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.section179Deduction">
    <ion-label>Section 179 deduction</ion-label>
    <ion-chip color="primary">{{ doc.section179Deduction }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherDeductions">
    <ion-label>Other deductions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherDeductions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.selfEmployment">
    <ion-label>Self-employment earnings (loss)</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.selfEmployment">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.credits">
    <ion-label>Credits</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.credits">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.foreignCountry">
    <ion-label>Foreign country</ion-label>
    <ion-chip color="primary">{{ doc.foreignCountry }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignTransactions">
    <ion-label>Foreign transactions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.foreignTransactions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.amtItems">
    <ion-label>Alternative minimum tax (AMT) items</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.amtItems">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.taxExemptIncome">
    <ion-label>Tax-exempt income and nondeductible expenses</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.taxExemptIncome">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.distributions">
    <ion-label>Distributions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.distributions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.otherInfo">
    <ion-label>Other information</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherInfo">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>


</ion-list>

    `
})
export class Tax1065K1Component implements OnInit {

    @Input()
    public doc: Tax1065K1;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
