import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Ltc } from '../../../models/tax1099Ltc';


@Component({
    selector: 'app-tax1099ltc',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.policyholderTin">
    <ion-label>POLICYHOLDER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.policyholderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.policyHolderNameAddress">
    <ion-label>Policyholder name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.policyHolderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ltcBenefits">
    <ion-label>Gross long-term care benefits paid</ion-label>
    <ion-chip color="primary">{{ doc.ltcBenefits }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.deathBenefits">
    <ion-label>Accelerated death benefits paid</ion-label>
    <ion-chip color="primary">{{ doc.deathBenefits }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.perDiem">
    <ion-label>Per diem</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.perDiem ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.reimbursedAmount">
    <ion-label>Reimbursed amount</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.reimbursedAmount ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.insuredId">
    <ion-label>INSURED'S taxpayer identification no.</ion-label>
    <ion-chip color="primary">{{ doc.insuredId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.insuredNameAddress">
    <ion-label>Insured name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.insuredNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.qualifiedContract">
    <ion-label>Qualified contract</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.qualifiedContract ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.chronicallyIll">
    <ion-label>Chronically ill</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.chronicallyIll ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.terminallyIll">
    <ion-label>Terminally ill</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.terminallyIll ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateCertified">
    <ion-label>Date certified (MM/DD/YY)</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateCertified ) }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099LtcComponent implements OnInit {

    @Input()
    public doc: Tax1099Ltc;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
