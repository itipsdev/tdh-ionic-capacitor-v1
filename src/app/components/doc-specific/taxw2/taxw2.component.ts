import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { TaxW2 } from '../../../models/taxW2';


@Component({
    selector: 'app-taxw2',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.employeeTin">
    <ion-label>Employee's social security number</ion-label>
    <ion-chip color="primary">{{ doc.employeeTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.employerTin">
    <ion-label>Employer identification number (EIN)</ion-label>
    <ion-chip color="primary">{{ doc.employerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.employerNameAddress">
    <ion-label>Employer's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.employerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.controlNumber">
    <ion-label>Control number</ion-label>
    <ion-chip color="primary">{{ doc.controlNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.employeeName">
    <ion-label>Employee name</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'IndividualName', doc.employeeName ) }}</pre></ion-item>

<ion-item *ngIf="doc.employeeAddress">
    <ion-label>Employee's address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'Address', doc.employeeAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.wages">
    <ion-label>Wages, tips, other compensation</ion-label>
    <ion-chip color="primary">{{ doc.wages }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.socialSecurityWages">
    <ion-label>Social security wages</ion-label>
    <ion-chip color="primary">{{ doc.socialSecurityWages }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.socialSecurityTaxWithheld">
    <ion-label>Social security tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.socialSecurityTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.medicareWages">
    <ion-label>Medicare wages and tips</ion-label>
    <ion-chip color="primary">{{ doc.medicareWages }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.medicareTaxWithheld">
    <ion-label>Medicare tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.medicareTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.socialSecurityTips">
    <ion-label>Social security tips</ion-label>
    <ion-chip color="primary">{{ doc.socialSecurityTips }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.allocatedTips">
    <ion-label>Allocated tips</ion-label>
    <ion-chip color="primary">{{ doc.allocatedTips }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dependentCareBenefit">
    <ion-label>Dependent care benefits</ion-label>
    <ion-chip color="primary">{{ doc.dependentCareBenefit }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonQualifiedPlan">
    <ion-label>Nonqualified plans</ion-label>
    <ion-chip color="primary">{{ doc.nonQualifiedPlan }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.codes">
    <ion-label>Box 12 codes and amounts</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.codes">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.statutory">
    <ion-label>Statutory employee</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.statutory ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.retirementPlan">
    <ion-label>Retirement plan</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.retirementPlan ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.thirdPartySickPay">
    <ion-label>Third-party sick pay</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.thirdPartySickPay ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.other">
    <ion-label>Box 14 other descriptions and amounts</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.other">
    <ion-chip slot="end" color="primary"> {{ o.description }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>

<ion-item *ngIf="doc.localTaxWithholding">
    <ion-label>Local tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'LocalTaxWithholding', doc.localTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class TaxW2Component implements OnInit {

    @Input()
    public doc: TaxW2;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
