import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { TaxW2G } from '../../../models/taxW2G';


@Component({
    selector: 'app-taxw2g',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER'S federal identification number</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.winnerNameAddress">
    <ion-label>Winner's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.winnerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.winnings">
    <ion-label>Reportable winnings</ion-label>
    <ion-chip color="primary">{{ doc.winnings }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateWon">
    <ion-label>Date won</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateWon ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.typeOfWager">
    <ion-label>Type of wager</ion-label>
    <ion-chip color="primary">{{ doc.typeOfWager }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.transaction">
    <ion-label>Transaction</ion-label>
    <ion-chip color="primary">{{ doc.transaction }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.race">
    <ion-label>Race</ion-label>
    <ion-chip color="primary">{{ doc.race }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.identicalWinnings">
    <ion-label>Winnings from identical wagers</ion-label>
    <ion-chip color="primary">{{ doc.identicalWinnings }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.cashier">
    <ion-label>Cashier</ion-label>
    <ion-chip color="primary">{{ doc.cashier }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.winnerTin">
    <ion-label>Winner's taxpayer identification no.</ion-label>
    <ion-chip color="primary">{{ doc.winnerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.window">
    <ion-label>Window</ion-label>
    <ion-chip color="primary">{{ doc.window }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.firstId">
    <ion-label>First I.D.</ion-label>
    <ion-chip color="primary">{{ doc.firstId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.secondId">
    <ion-label>Second I.D.</ion-label>
    <ion-chip color="primary">{{ doc.secondId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payerState">
    <ion-label>State</ion-label>
    <ion-chip color="primary">{{ doc.payerState }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateWinnings">
    <ion-label>State winnings</ion-label>
    <ion-chip color="primary">{{ doc.stateWinnings }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithheld">
    <ion-label>State income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.stateTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.localWinnings">
    <ion-label>Local winnings</ion-label>
    <ion-chip color="primary">{{ doc.localWinnings }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.localTaxWithheld">
    <ion-label>Local income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.localTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.localityName">
    <ion-label>Name of locality</ion-label>
    <ion-chip color="primary">{{ doc.localityName }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payerStateId">
    <ion-label>PAYER's state identification no.</ion-label>
    <ion-chip color="primary">{{ doc.payerStateId }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class TaxW2GComponent implements OnInit {

    @Input()
    public doc: TaxW2G;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
