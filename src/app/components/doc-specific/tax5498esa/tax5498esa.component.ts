import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax5498Esa } from '../../../models/tax5498Esa';


@Component({
    selector: 'app-tax5498esa',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.issuerNameAddress">
    <ion-label>Issuer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.issuerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.issuerTin">
    <ion-label>TRUSTEE'S/ISSUER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.issuerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.participantTin">
    <ion-label>BENEFICIARY'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.participantTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.beneficiaryNameAddress">
    <ion-label>Beneficiary's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.beneficiaryNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.coverdellEsaContributions">
    <ion-label>Coverdell ESA contributions</ion-label>
    <ion-chip color="primary">{{ doc.coverdellEsaContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rolloverContributions">
    <ion-label>Rollover contributions</ion-label>
    <ion-chip color="primary">{{ doc.rolloverContributions }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax5498EsaComponent implements OnInit {

    @Input()
    public doc: Tax5498Esa;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
