import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098C } from '../../../models/tax1098C';


@Component({
    selector: 'app-tax1098c',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.doneeNameAddress">
    <ion-label>Donee's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.doneeNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.doneeTin">
    <ion-label>DONEE'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.doneeTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.donorTin">
    <ion-label>DONOR'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.donorTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.donorNameAddress">
    <ion-label>Donor's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.donorNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.dateOfContribution">
    <ion-label>Date of contribution</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfContribution ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.odometerMileage">
    <ion-label>Odometer mileage</ion-label>
    <ion-chip color="primary">{{ doc.odometerMileage }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.carYear">
    <ion-label>Year</ion-label>
    <ion-chip color="primary">{{ doc.carYear }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.make">
    <ion-label>Make</ion-label>
    <ion-chip color="primary">{{ doc.make }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.model">
    <ion-label>Model</ion-label>
    <ion-chip color="primary">{{ doc.model }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.vin">
    <ion-label>Vehicle or other identification number</ion-label>
    <ion-chip color="primary">{{ doc.vin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.armsLengthTransaction">
    <ion-label>Donee certifies that vehicle was sold in arm's length transaction to unrelated party</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.armsLengthTransaction ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateOfSale">
    <ion-label>Date of sale</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfSale ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossProceeds">
    <ion-label>Gross proceeds from sale (see instructions)</ion-label>
    <ion-chip color="primary">{{ doc.grossProceeds }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.notTransferredBefore">
    <ion-label>Donee certifies that vehicle will not be transferred for money, other property, or services before completion of material improvements or significant intervening use</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.notTransferredBefore ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.needyIndividual">
    <ion-label>Donee certifies that vehicle is to be transferred to a needy individual for significantly below fair market value in furtherance of donee's charitable purpose</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.needyIndividual ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.descriptionOfImprovements">
    <ion-label>Donee certifies the following detailed description of material improvements or significant intervening use and duration of use</ion-label>
    <ion-chip color="primary">{{ doc.descriptionOfImprovements }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.goodsInExchange">
    <ion-label>Did you provide goods or services in exchange for the vehicle? Yes</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.goodsInExchange ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.valueOfExchange">
    <ion-label>Value of goods and services provided in exchange for the vehicle</ion-label>
    <ion-chip color="primary">{{ doc.valueOfExchange }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.intangibleReligious">
    <ion-label>If this box is checked, donee certifies that the goods and services consisted solely of intangible religious benefits</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.intangibleReligious ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.descriptionOfGoods">
    <ion-label>Describe the goods and services, if any, that were provided.</ion-label>
    <ion-chip color="primary">{{ doc.descriptionOfGoods }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.maxDeductionApplies">
    <ion-label>Under the law, the donor may not claim a deduction of more than $500 for this vehicle if this box is checked</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.maxDeductionApplies ) }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1098CComponent implements OnInit {

    @Input()
    public doc: Tax1098C;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
