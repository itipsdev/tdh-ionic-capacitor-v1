import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Q } from '../../../models/tax1099Q';


@Component({
    selector: 'app-tax1099q',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER'S/TRUSTEE'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossDistribution">
    <ion-label>Gross distribution</ion-label>
    <ion-chip color="primary">{{ doc.grossDistribution }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.earnings">
    <ion-label>Earnings</ion-label>
    <ion-chip color="primary">{{ doc.earnings }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.basis">
    <ion-label>Basis</ion-label>
    <ion-chip color="primary">{{ doc.basis }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.trusteeToTrustee">
    <ion-label>Trustee-to-trustee transfer</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.trusteeToTrustee ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.tuitionPlanPrivate">
    <ion-label>Qualified tuition plan - Private</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.tuitionPlanPrivate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.tuitionPlanPublic">
    <ion-label>Qualified tuition plan - Public</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.tuitionPlanPublic ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.coverdellEsa">
    <ion-label>Coverdell ESA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.coverdellEsa ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientIsNotBeneficiary">
    <ion-label>If this box is checked, the recipient is not the designated beneficiary</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.recipientIsNotBeneficiary ) }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099QComponent implements OnInit {

    @Input()
    public doc: Tax1099Q;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
