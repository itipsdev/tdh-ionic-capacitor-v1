import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1120SK1 } from '../../../models/tax1120SK1';


@Component({
    selector: 'app-tax1120sk1',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.finalK1">
    <ion-label>Final K-1</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.finalK1 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.amendedK1">
    <ion-label>Amended K-1</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.amendedK1 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fiscalYearBegin">
    <ion-label>Fiscal year begin date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearBegin ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fiscalYearEnd">
    <ion-label>Fiscal year end date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearEnd ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.corporationTin">
    <ion-label>Corporation's employer identification number</ion-label>
    <ion-chip color="primary">{{ doc.corporationTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.corporationNameAddress">
    <ion-label>Corporation's name, address, city, state, and ZIP code</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.corporationNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.irsCenter">
    <ion-label>IRS Center where corporation filed return</ion-label>
    <ion-chip color="primary">{{ doc.irsCenter }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.shareholderTin">
    <ion-label>Shareholder's identifying number</ion-label>
    <ion-chip color="primary">{{ doc.shareholderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.shareholderNameAddress">
    <ion-label>Shareholder's name, address, city, state, and ZIP code</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.shareholderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.percentOwnership">
    <ion-label>Shareholder's percentage of stock ownership for tax year</ion-label>
    <ion-chip color="primary">{{ doc.percentOwnership }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryIncome">
    <ion-label>Ordinary business income (loss)</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netRentalRealEstateIncome">
    <ion-label>Net rental real estate income (loss</ion-label>
    <ion-chip color="primary">{{ doc.netRentalRealEstateIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherRentalIncome">
    <ion-label>Other net rental income (loss)</ion-label>
    <ion-chip color="primary">{{ doc.otherRentalIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.interestIncome">
    <ion-label>Interest income</ion-label>
    <ion-chip color="primary">{{ doc.interestIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryDividends">
    <ion-label>Ordinary dividends</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedDividends">
    <ion-label>Qualified dividends</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.royalties">
    <ion-label>Royalties</ion-label>
    <ion-chip color="primary">{{ doc.royalties }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netShortTermGain">
    <ion-label>Net short-term capital gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.netShortTermGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netLongTermGain">
    <ion-label>Net long-term capital gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.netLongTermGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.collectiblesGain">
    <ion-label>Collectibles (28%) gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.collectiblesGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrecaptured1250Gain">
    <ion-label>Unrecaptured section 1250 gain</ion-label>
    <ion-chip color="primary">{{ doc.unrecaptured1250Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.net1231Gain">
    <ion-label>Net section 1231 gain (loss)</ion-label>
    <ion-chip color="primary">{{ doc.net1231Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherIncome">
    <ion-label>Other income (loss)</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherIncome">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.section179Deduction">
    <ion-label>Section 179 deduction</ion-label>
    <ion-chip color="primary">{{ doc.section179Deduction }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherDeductions">
    <ion-label>Other deductions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherDeductions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.credits">
    <ion-label>Credits</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.credits">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.foreignTransactions">
    <ion-label>Foreign transactions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.foreignTransactions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.foreignCountry">
    <ion-label>Foreign country</ion-label>
    <ion-chip color="primary">{{ doc.foreignCountry }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.amtItems">
    <ion-label>Alternative minimum tax (AMT) items</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.amtItems">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.basisItems">
    <ion-label>Items affecting shareholder basis</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.basisItems">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.otherInfo">
    <ion-label>Other information</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherInfo">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>


</ion-list>

    `
})
export class Tax1120SK1Component implements OnInit {

    @Input()
    public doc: Tax1120SK1;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
