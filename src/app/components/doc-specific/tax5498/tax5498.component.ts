import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax5498 } from '../../../models/tax5498';


@Component({
    selector: 'app-tax5498',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.issuerNameAddress">
    <ion-label>Issuer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.issuerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.issuerTin">
    <ion-label>TRUSTEE's or ISSUER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.issuerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.participantTin">
    <ion-label>PARTICIPANT's TIN</ion-label>
    <ion-chip color="primary">{{ doc.participantTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.participantNameAddress">
    <ion-label>Participant's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.participantNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.iraContributions">
    <ion-label>IRA contributions</ion-label>
    <ion-chip color="primary">{{ doc.iraContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rolloverContributions">
    <ion-label>Rollover contributions</ion-label>
    <ion-chip color="primary">{{ doc.rolloverContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rothIraConversion">
    <ion-label>Roth IRA conversion amount</ion-label>
    <ion-chip color="primary">{{ doc.rothIraConversion }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recharacterizedContributions">
    <ion-label>Recharacterized contributions</ion-label>
    <ion-chip color="primary">{{ doc.recharacterizedContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fairMarketValue">
    <ion-label>Fair market value of account</ion-label>
    <ion-chip color="primary">{{ doc.fairMarketValue }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.lifeInsuranceCost">
    <ion-label>Life insurance cost included in box 1</ion-label>
    <ion-chip color="primary">{{ doc.lifeInsuranceCost }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ira">
    <ion-label>IRA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.ira ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.sep">
    <ion-label>SEP</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.sep ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.simple">
    <ion-label>SIMPLE</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.simple ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rothIra">
    <ion-label>ROTHIRA</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.rothIra ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.sepContributions">
    <ion-label>SEP contributions</ion-label>
    <ion-chip color="primary">{{ doc.sepContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.simpleContributions">
    <ion-label>SIMPLE contributions</ion-label>
    <ion-chip color="primary">{{ doc.simpleContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rothIraContributions">
    <ion-label>Roth IRA contributions</ion-label>
    <ion-chip color="primary">{{ doc.rothIraContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rmdNextYear">
    <ion-label>If checked, required minimum distribution for 2020</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.rmdNextYear ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rmdDate">
    <ion-label>RMD date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.rmdDate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rmdAmount">
    <ion-label>RMD amount</ion-label>
    <ion-chip color="primary">{{ doc.rmdAmount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.postponedContribution">
    <ion-label>Postponed contribution</ion-label>
    <ion-chip color="primary">{{ doc.postponedContribution }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.postponedYear">
    <ion-label>Year</ion-label>
    <ion-chip color="primary">{{ doc.postponedYear }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.postponedCode">
    <ion-label>Code</ion-label>
    <ion-chip color="primary">{{ doc.postponedCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.repayments">
    <ion-label>Repayments</ion-label>
    <ion-chip color="primary">{{ doc.repayments }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.repayCode">
    <ion-label>Code</ion-label>
    <ion-chip color="primary">{{ doc.repayCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fmvSpecifiedAssets">
    <ion-label>FMV of certain specified assets</ion-label>
    <ion-chip color="primary">{{ doc.fmvSpecifiedAssets }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.specifiedCodes">
    <ion-label>Code(s)</ion-label>
    <ion-chip color="primary">{{ doc.specifiedCodes }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax5498Component implements OnInit {

    @Input()
    public doc: Tax5498;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
