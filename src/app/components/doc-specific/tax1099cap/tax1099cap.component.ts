import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Cap } from '../../../models/tax1099Cap';


@Component({
    selector: 'app-tax1099cap',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.corporationNameAddress">
    <ion-label>Corporation's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.corporationNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.corporationTin">
    <ion-label>CORPORATION'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.corporationTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.shareholderTin">
    <ion-label>SHAREHOLDER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.shareholderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.shareholderNameAddress">
    <ion-label>Shareholder's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.shareholderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateOfSale">
    <ion-label>Date of sale or exchange</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfSale ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.aggregateAmount">
    <ion-label>Aggregate amount received</ion-label>
    <ion-chip color="primary">{{ doc.aggregateAmount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.numberOfShares">
    <ion-label>Number of shares exchanged</ion-label>
    <ion-chip color="primary">{{ doc.numberOfShares }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stockClasses">
    <ion-label>Classes of stock exchanged</ion-label>
    <ion-chip color="primary">{{ doc.stockClasses }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099CapComponent implements OnInit {

    @Input()
    public doc: Tax1099Cap;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
