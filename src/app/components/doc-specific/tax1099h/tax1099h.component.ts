import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099H } from '../../../models/tax1099H';


@Component({
    selector: 'app-tax1099h',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.issuerNameAddress">
    <ion-label>Issuer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.issuerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.issuerTin">
    <ion-label>ISSUER'S/PROVIDER'S federal identification number</ion-label>
    <ion-chip color="primary">{{ doc.issuerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT'S identification number</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.advancePayments">
    <ion-label>Amount of HCTC advance payments</ion-label>
    <ion-chip color="primary">{{ doc.advancePayments }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.numberOfMonths">
    <ion-label>Number of months HCTC advance payments and reimbursement credits paid to you</ion-label>
    <ion-chip color="primary">{{ doc.numberOfMonths }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payments">
    <ion-label>Payments by month</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.payments">
    <ion-chip slot="end" color="primary"> {{ o.month }}  {{ o.amount }} </ion-chip> 
</ion-item>


</ion-list>

    `
})
export class Tax1099HComponent implements OnInit {

    @Input()
    public doc: Tax1099H;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
