import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098E } from '../../../models/tax1098E';


@Component({
    selector: 'app-tax1098e',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.lenderNameAddress">
    <ion-label>Lender's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.lenderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.lenderTin">
    <ion-label>LENDER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.lenderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.borrowerTin">
    <ion-label>BORROWER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.borrowerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.borrowerNameAddress">
    <ion-label>Borrower's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.borrowerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.studentLoanInterest">
    <ion-label>Student loan interest received by lender</ion-label>
    <ion-chip color="primary">{{ doc.studentLoanInterest }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.box1ExcludesFees">
    <ion-label>If checked, box 1 does not include loan origination fee made before September 1, 2004</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.box1ExcludesFees ) }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1098EComponent implements OnInit {

    @Input()
    public doc: Tax1098E;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
