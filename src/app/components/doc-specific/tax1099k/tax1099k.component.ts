import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099K } from '../../../models/tax1099K';


@Component({
    selector: 'app-tax1099k',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.filerNameAddress">
    <ion-label>Filer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.filerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.paymentSettlementEntity">
    <ion-label>Check to indicate if FILER is a (an): Payment settlement entity (PSE)</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.paymentSettlementEntity ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.electronicPaymentFacilitator">
    <ion-label>Check to indicate if FILER is a (an): Electronic payment facilitator (EPF)/Other third party</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.electronicPaymentFacilitator ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.paymentCard">
    <ion-label>Check to indicate transactions reported are: Payment card</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.paymentCard ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.thirdPartyNetwork">
    <ion-label>Check to indicate transactions reported are: Third party network</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.thirdPartyNetwork ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payeeNameAddress">
    <ion-label>Payee's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.payeeNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.pseName">
    <ion-label>PSE's name   </ion-label>
    <ion-chip color="primary">{{ doc.pseName }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.filerTin">
    <ion-label>FILER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.filerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payeeFederalId">
    <ion-label>PAYEE'S taxpayer identification no.</ion-label>
    <ion-chip color="primary">{{ doc.payeeFederalId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossAmount">
    <ion-label>Gross amount of payment card/third party network transactions</ion-label>
    <ion-chip color="primary">{{ doc.grossAmount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.cardNotPresent">
    <ion-label>Card Not Present Transactions</ion-label>
    <ion-chip color="primary">{{ doc.cardNotPresent }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.merchantCategoryCode">
    <ion-label>Merchant category code</ion-label>
    <ion-chip color="primary">{{ doc.merchantCategoryCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.numberOfTransactions">
    <ion-label>Number of purchase transactions</ion-label>
    <ion-chip color="primary">{{ doc.numberOfTransactions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.monthAmounts">
    <ion-label>Monthly amounts</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.monthAmounts">
    <ion-chip slot="end" color="primary"> {{ o.month }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>

<ion-item *ngIf="doc.psePhone">
    <ion-label>PSE's phone number</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'TelephoneNumberPlusExtension', doc.psePhone ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099KComponent implements OnInit {

    @Input()
    public doc: Tax1099K;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
