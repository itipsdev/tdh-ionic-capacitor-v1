import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component( {
    selector: 'app-scan-link',
    templateUrl: './scan-link.component.html',
    styleUrls: [ './scan-link.component.scss' ],
} )
export class ScanLinkComponent implements OnInit, AfterViewInit {

    @ViewChild(
        'wrapperDiv',
        { static: false }
    )
    public divView: ElementRef;

    @ViewChild(
        'canvasElement',
        { static: false }
    )
    public canvasView: ElementRef;

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        console.log( 'ScanLinkComponent ngAfterViewInit' );

        console.log( 'canvas ' + this.canvasView.nativeElement.width );

        console.log( 'div ' + this.divView.nativeElement.clientWidth );

    }

}
