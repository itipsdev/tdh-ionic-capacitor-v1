import { Component, OnDestroy, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';
import { ActivatedRoute } from '@angular/router';
import { TaxDocListItem } from '../../models/TaxDocListItem';
import { DescriptionValue } from '../../models/descriptionValue';
import {LoggerService} from '../../services/logger.service';

@Component( {
    selector: 'app-doc',
    templateUrl: './doc.component.html',
    styleUrls: [ './doc.component.scss' ],
} )
export class DocComponent implements OnInit, OnDestroy {

    // Index of this document in document list
    public docId: number;

    public doc: TaxDocListItem;

    constructor(
        private global: GlobalDataService,
        private logger: LoggerService,
        private activatedRoute: ActivatedRoute
    ) {


    }

    ngOnInit( ) {

        // Subscribe ?
        this.docId = this.activatedRoute.snapshot.params['docId'];

        this.logger.trace( `DocComponent ngOnInit ${this.docId}` );

        const arr: TaxDocListItem[] = this.global.getTaxDocs( );

        this.logger.debugAny( arr );

        this.doc = arr[ this.docId ];

    }

    ngOnDestroy( ): void {

    }

}
