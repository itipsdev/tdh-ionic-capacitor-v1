import { Injectable } from '@angular/core';
import { LoggerService } from './logger.service';
import { TaxDocListItem } from '../models/TaxDocListItem';
import {Capacitor, Plugins} from '@capacitor/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import { UUID } from 'angular2-uuid';

@Injectable( {
    providedIn: 'root'
} )
export class LocalStorageService {

    // ==============================================================
    // Local storage keys
    // ==============================================================
    private DEVICE_GUID_KEY      = 'device_guid';

    private TAX_DOCS_STORAGE_KEY = 'tax_docs_data';

    private DEVICE_LOCKER_KEY    = 'device_locker';


    // ==============================================================
    // Local storage values
    // ==============================================================
    public deviceGuid: string;
    public deviceGuidSubject
        = new BehaviorSubject<string>('' );

    public lockerNumber: string;
    public deviceLockerSubject
        = new BehaviorSubject<string>( '' );

    // Data
    public taxDocs: TaxDocListItem[] = [];
    // Subject and Observable to provide asynchronous access to data
    public taxDocListSubject
        = new BehaviorSubject<TaxDocListItem[]>([]);


    constructor(
        private logger: LoggerService
    ) {
        this.initializeStorage( );
    }

    public initializeStorage( ) {

        this.logger.trace( `LocalStorageService initializeStorage` );

        if ( Capacitor.isPluginAvailable( 'Storage' ) ) {

            this.logger.trace( `LocalStorageService initializeStorage Storage true` );

            this.getDeviceGuid( );

            this.getTaxDocsList( );

        } else {

            this.logger.trace( `LocalStorageService initializeStorage Storage false` );

        }

    }

    public getTaxDocsList( ) {

        this.logger.trace( 'getTaxDocsList' );

        Plugins.Storage
            .get( { key: this.TAX_DOCS_STORAGE_KEY } )
            .then(
                ( pair: any ) => {

                    this.logger.trace( 'LocalStorageService retrieveTaxDataFromStorage then' );

                    this.taxDocs = [];

                    if ( pair != null ) {

                        if ( pair.value != null ) {

                            const obj: any = JSON.parse( pair.value );

                            this.taxDocs = obj;

                        }

                    }

                    this.logger.trace( 'LocalStorageService retrieveTaxDataFromStorage done' );

                    this.taxDocListSubject.next( this.taxDocs );

                },
                ( error ) => {

                    this.logger.trace( 'LocalStorageService retrieveTaxDataFromStorage error ' + error );

                    this.taxDocs = [];

                    this.taxDocListSubject.next( this.taxDocs );

                }

            );

    }

    public getTaxDoc( ) {

        this.logger.trace( 'getTaxDoc' );

    }

    public addTaxDoc(
        taxDocListItem: TaxDocListItem
    ): number {

        this.logger.trace( 'LocalStorageService addTaxDoc' );

        this.taxDocs.push( taxDocListItem );

        this.taxDocListSubject.next( this.taxDocs );

        this.saveTaxDataToStorage( this.taxDocs );

        // Return the array element number of new doc
        return this.taxDocs.length - 1;

    }

    public updateTaxDocHubItemNumber(
        itemSha: string,
        itemNumber: string
    ) {

        for ( let item of this.taxDocs ) {

            if ( itemSha == item.sha ) {

                item.docId = itemNumber;

            }

        }

        this.saveTaxDataToStorage(
            this.taxDocs
        );

    }

    public saveTaxDataToStorage(
        taxDocs: TaxDocListItem[]
    ) {

        this.logger.trace( 'LocalStorageService saveTaxDataToStorage begin' );

        // Data as JSON
        const data: string =
            JSON.stringify( taxDocs, null, 4 );

        // Save
        Plugins.Storage.set(
            {
                key: this.TAX_DOCS_STORAGE_KEY,
                value: data
            }
        ).then(
            ( ) => {

                this.logger.trace( 'LocalStorageService saveTaxDataToStorage saved' );

            },
            ( error ) => {

                this.logger.trace( 'LocalStorageService saveTaxDataToStorage error ' + error );

            }
        );

    }

    public updateTaxDoc( ) {

        this.logger.trace( 'updateTaxDoc' );

    }

    public deleteTaxDoc( ) {

        this.logger.trace( 'deleteTaxDoc' );

    }

    public clearTaxDataStorage( ) {

        this.logger.trace( 'LocalStorageService clearTaxDataStorage begin' );


        this.taxDocs = [];

        this.taxDocListSubject.next( this.taxDocs );

        this.saveTaxDataToStorage( this.taxDocs );

        // // Save
        // Plugins.Storage.remove(
        //     {
        //         key:   this.TAX_DOCS_STORAGE_KEY
        //     }
        // ).then(
        //     ( ) => {
        //         this.logger.trace( 'LocalStorageService clearTaxDataStorage clear' );
        //     },
        //     ( error ) => {
        //         this.logger.trace( 'LocalStorageService clearTaxDataStorage error ' + error );
        //     }
        // );

    }

    public getLockerNumber(

    ): Observable<string> {

        this.logger.trace( `LocalStorageService getLockerNumber` );

        const subject: Subject<string> = new Subject<string>( );

        if ( this.lockerNumber ) {

            // Make sure .next not called before observable is returned
            setTimeout(
                ( ) => {
                    subject.next( this.lockerNumber );
                },
                150
            );

        } else {

            Plugins.Storage
                .get( { key: this.DEVICE_LOCKER_KEY } )
                .then(
                    ( obj ) => {

                        this.lockerNumber = obj.value;

                        this.logger.dump( 'STORED _lockerNumber', this.lockerNumber );

                        if ( this.lockerNumber ) {

                            subject.next( this.lockerNumber );

                        } else {

                            // Create.  TODO Retrieve from data store.

                            this.lockerNumber = String( Math.floor( Math.random( ) * Math.floor( 10000000 ) ) );

                            this.logger.dump( 'NEW _lockerNumber', this.lockerNumber );

                            // Save
                            Plugins.Storage.set(
                                {
                                    key:   this.DEVICE_LOCKER_KEY,
                                    value: this.lockerNumber
                                }
                            ).then(
                                ( ) => {
                                    subject.next( this.lockerNumber );
                                }
                            );

                        }
                    }
                );

        }

        return subject.asObservable( );

    }

    public getDeviceGuid( ) {

        if ( this.deviceGuid ) {

            // Already in memory
            this.deviceGuidSubject.next( this.deviceGuid  )

        } else {

            Plugins.Storage
                .get( { key: this.DEVICE_GUID_KEY } )
                .then(
                    ( obj ) => {

                        // Check if already stored

                        this.deviceGuid = obj['value'];

                        this.logger.dump( 'STORED _deviceGuid', this.deviceGuid );

                        if ( this.deviceGuid ) {

                            this.deviceGuidSubject.next( this.deviceGuid );

                        } else {

                            // If not already stored, create

                            // Create
                            this.deviceGuid = UUID.UUID( );

                            this.logger.dump( 'NEW _deviceGuid', this.deviceGuid );

                            // Save
                            Plugins.Storage.set(
                                {
                                    key:   this.DEVICE_GUID_KEY,
                                    value: this.deviceGuid
                                }
                            ).then(
                                ( ) => {
                                    this.deviceGuidSubject.next( this.deviceGuid );
                                }
                            );

                        }
                    }
                );

        }

    }

}
