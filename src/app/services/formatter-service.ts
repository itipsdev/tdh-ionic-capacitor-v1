import { Injectable } from '@angular/core';

@Injectable( {
    providedIn: 'root'
} )
export class FormatterService {

    public format5(
        objType: string,
        obj1: any,
        obj2: any,
        obj3: any,
        obj4: any
    ): string {

        switch ( objType ) {

            case 'LabeledDollar':
                return this.formatLabeledDollar( obj1, obj2, obj3 );

            // ============================================
            // Security details items
            // ============================================

            case 'grossOrNet':
                return this.formatGrossOrNet( obj2, obj1, obj3 );

            case 'longOrShort':
                return this.formatLongOrShort( obj2, obj1, obj3 );

            default:
                return '' + objType + '-5';

        }

    }

    public format4(
        objType: string,
        obj1: any,
        obj2: any,
        obj3: any
    ): string {

        switch ( objType ) {


            case 'NamePhone':
                return '' + obj1 + ' ' + obj2;

            case 'StateStateId':
                return '' + obj1 + '/' + obj2;

            default:
                return '' + objType + '-4';

        }

    }

    public format3(
        objType: string,
        obj: any,
        elNum: any
    ): string {

        switch( objType ) {

            case 'CodeAmount':
                return this.formatCodeAmount2( obj, elNum );

            case 'CodeLetter':
                return this.formatCodeLetter2( obj, elNum );

            case 'Locality':
                return this.formatLocality2( obj, elNum );

            case 'LocalIncome':
                return this.formatLocalIncome2( obj, elNum );

            case 'LocalWH':
                return this.formatLocalWH2( obj, elNum );

            case 'MonthAmount':
                return this.formatMonthAmount2( obj, elNum );

            case 'OtherPara':
                return this.formatOtherPara2( obj, elNum );

            case 'StateCode':
                return this.formatStateCode2( obj, elNum );

            case 'StateCodeId':
                return this.formatStateCodeId2( obj, elNum );

            case 'StateId':
                return this.formatStateId2( obj, elNum );

            case 'StateIncome':
                return this.formatStateIncome2( obj, elNum );

            case 'StateWH':
                return this.formatStateWH2( obj, elNum );

            // ============================================
            // Security details items
            // ============================================

            case 'accruedMarketDiscount':
                return this.formatAccruedMarketDiscount( obj, elNum );

            case 'basisReported':
                return this.formatBasisReported( obj, elNum );

            case 'checkboxOnForm8949':
                return this.formatCheckboxOnForm8949SalesPrice( obj, elNum );

            case 'collectible':
                return this.formatCollectible( obj, elNum );

            case 'costBasis':
                return this.formatCostBasis( obj, elNum );

            case 'dateAcquired':
                return this.formatDateAcquired( obj, elNum );

            case 'dateOfSale':
                return this.formatDateOfSale( obj, elNum );

            case 'foreignAccountTaxCompliance':
                return this.formatForeignAccountTaxCompliance( obj, elNum );

            case 'lossNotAllowed':
                return this.formatLossNotAllowed( obj, elNum );

            case 'noncoveredSecurity':
                return this.formatNoncoveredSecurity( obj, elNum );

            case 'ordinary':
                return this.formatOrdinary( obj, elNum );

            case 'qof':
                return this.formatQof( obj, elNum );

            case 'saleDescription':
                return this.formatSaleDescription( obj, elNum );

            case 'salesPrice':
                return this.formatSalesPrice( obj, elNum );

            case 'washSaleLossDisallowed':
                return this.formatWashSaleLossDisallowed( obj, elNum );

            default:
                console.log( objType );
                return objType;

        }

    }

    public format(
        objType: string,
        obj: any
    ): string {

        switch( objType ) {

            case 'array':
                return this.formatArray( obj );

            case 'Address':
                return this.formatAddress( obj );

            case 'Boolean':
                return this.formatBoolean( obj );

            case 'CityStateZip':
                return this.formatCityStateZip( obj );

            case 'CodeAmount':
                return this.formatCodeAmount( obj );

            case 'Concat':
                return this.formatConcat( obj );

            case 'DescriptionAmount':
                return this.formatDescriptionAmount( obj );

            case 'FirstInit':
                return this.formatFirstInit( obj );

            case 'HealthInsuranceCoveredIndividual':
                return this.formatHealthInsuranceCoveredIndividual( obj );

            case 'HealthInsuranceCoverage':
                return this.formatHealthInsuranceCoverage( obj );

            case 'HealthInsuranceMarketplaceCoveredIndividual':
                return this.formatHealthInsuranceMarketplaceCoveredIndividual( obj );

            case 'IndividualName':
                return this.formatIndividualName( obj );

            case 'integer':
                return this.formatInteger( obj );

            case 'LastName':
                return this.formatLastName( obj );

            case 'LocalTaxWithholding':
                return this.formatLocalTaxWithholding( obj );

            case 'MonthAmount':
                return this.formatMonthAmount( obj );

            case 'Name':
                return this.formatName( obj );

            case 'NameAddress':
                return this.formatNameAddress( obj );

            case 'NameAddressPhone':
            case 'NameEtc':
                return this.formatNameAddressPhone( obj );

            case 'number':
                return this.formatNumber( obj );

            case 'OfferOfHealthInsuranceCoverage':
                return this.formatOfferOfHealthInsuranceCoverage( obj );

            case 'SecurityDetail':
                return this.formatSecurityDetail( obj );

            case 'StateTaxWithholding':
                return this.formatStateTaxWithholding( obj );

            case 'StreetAddress':
                return this.formatStreetAddress( obj );

            case 'Suffix':
                return this.formatSuffix( obj );

            case 'TelephoneNumberPlusExtension':
                return this.formatTelephoneNumberPlusExtension( obj );

            case 'Timestamp':
                return this.formatTimestamp( obj );

            default:
                console.log( objType );
                return objType;

        }

    }

    public formatAddress(
        obj
    ): string {

        let value: string = '';

        if ( obj.line1 ) {
            value += obj.line1 + '\n';
        }

        if ( obj.line2 ) {
            value += obj.line2 + '\n';
        }

        // if ( obj.line3 ) {
        //     value += obj.line3 + '\n';
        // }

        const cityStateZip = this.formatCityStateZip( obj );
        if ( cityStateZip ) {
            value += cityStateZip;
        }

        return value;

    }

    public formatArray(
        obj
    ): string {

        return '+ array ';

    }

    public formatBoolean(
        obj
    ): string {

        if ( obj ) {
            return 'X';
        }

        return '';

    }

    public formatCityStateZip(
        obj
    ): string {

        let value: string = '';

        if ( obj.city ) {
            value += obj.city;
        }

        if ( obj.state ) {
            value += ', ' + obj.state;
        }

        const zip = this.formatZip( obj );
        if ( zip ) {
            value += ' ' + zip;
        }

        return value;

    }

    public formatCodeAmount(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += pair.code + ' ' + this.formatNumber( pair.amount );

        }

        return value;

    }

    public formatCodeAmount2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].amount );

        return value;

    }

    public formatCodeLetter2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].code;

        return value;

    }

    public formatConcat(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let item of obj ) {

            value += item;

        }

        return value;

    }

    public formatDate(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        return value;

    }

    public formatDescriptionAmount(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += pair.description + ' ' + this.formatNumber( pair.amount );

        }

        return value;

    }

    public formatFirstInit(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.first ) {
            value += obj.first + ' ';
        }

        if ( obj.middle ) {
            value += obj.middle;
        }

        return value;

    }

    public formatHealthInsuranceCoverage(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.enrollmentPremium ) {
                value += item.enrollmentPremium + ' ';
            }

            if ( item.slcspPremium ) {
                value += item.slcspPremium + '\n';
            }

            if ( item.advancePremiumTaxCreditPayment ) {
                value += item.advancePremiumTaxCreditPayment + ' ';
            }

            if ( item.month ) {
                value += item.month + '\n';
            }

        }

        return value;

    }

    public formatHealthInsuranceCoveredIndividual(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.name ) {
                value += this.formatIndividualName( item.name ) + ' ';
            }

            if ( item.tin ) {
                value += item.tin + '\n';
            }

            if ( item.dateOfBirth ) {
                value += item.dateOfBirth + ' ';
            }

            if ( item.coveredAt12Months ) {
                value += item.coveredAt12Months + '\n';
            }

            if ( item.coveredMonths ) {
                for ( let mo of item.coveredMonths ) {
                    value += mo + '\n';
                }
            }

        }

        return value;

    }

    public formatHealthInsuranceMarketplaceCoveredIndividual(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.name ) {
                value += item.name + ' ';
            }

            if ( item.tin ) {
                value += item.tin + '\n';
            }

            if ( item.dateOfBirth ) {
                value += item.dateOfBirth + ' ';
            }

            if ( item.coveredAt12Months ) {
                value += item.coveredAt12Months + '\n';
            }

            if ( item.coveredMonths ) {
                for ( let mo of item.coveredMonths ) {
                    value += mo + '\n';
                }
            }

        }

        return value;

    }

    public formatIndividualName(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.first ) {
            value += obj.first + ' ';
        }

        if ( obj.middle ) {
            value += obj.middle + ' ';
        }

        if ( obj.last ) {
            value += obj.last + ' ';
        }

        if ( obj.suffix ) {
            value += obj.suffix;
        }

        return value;

    }

    public formatInteger(
        obj
    ): string {

        let value: string = '';

        value = String( obj );

        return value;

    }

    public formatLabeledDollar(
        label: string,
        dollarValue: number,
        totalLength: number
    ) {

        const formattedDollarValue: string = this.formatNumber( dollarValue );

        let numSpaces: number = totalLength - label.length - formattedDollarValue.length;
        if ( numSpaces < 0 ) {
            numSpaces = 0;
        }

        const spaces: string = ' '.repeat( numSpaces );

        const value: string =
            label +
            spaces +
            formattedDollarValue;

        return value;

    }

    public formatLastName(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.last ) {
            value += obj.last;
        }

        return value;

    }

    public formatLocality2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].localityName;

        return value;

    }

    public formatLocalIncome2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].localIncome );

        return value;

    }

    public formatLocalWH2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].localTaxWithheld );

        return value;

    }

    public formatLocalTaxWithholding(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.localityName ) {
                value += 'Locality name ........ ' + item.localityName + '\n';
            }

            if ( item.localTaxWithheld ) {
                value += 'Local tax withheld ... ' + item.localTaxWithheld + '\n';
            }

            if ( item.state ) {
                value += 'State ................ ' + item.state + '\n';
            }

            if ( item.localIncome ) {
                value += 'Local income ......... ' + item.localIncome + '\n';
            }

        }

        return value;

    }

    public formatMonthAmount(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += `${pair.month}  ${pair.amount}`;

        }

        return value;

    }

    public formatMonthAmount2(
        obj,
        key
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            if ( pair.month === key ) {
                value = this.formatNumber( pair.amount );
                break;
            }

        }

        return value;

    }

    public formatName(
        obj
    ): string {

        let value: string = '';

        if ( obj.name1 ) {
            value += obj.name1;
        }

        return value;

    }

    public formatNameAddress(
        obj
    ): string {

        let value: string = '';

        if ( obj.name1 ) {
            value += obj.name1 + '\n';
        }

        if ( obj.name2 ) {
            value += obj.name2 + '\n';
        }

        const address: string = this.formatAddress( obj );
        if ( address ) {
            value += address;
        }

        return value;
    }

    public formatNameAddressPhone(
        obj
    ): string {

        let value: string = '';

        const address: string = this.formatNameAddress( obj );
        if ( address ) {
            value += address;
        }

        return value;

    }

    public formatNumber(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        value = String( obj.toFixed(2) );

        return value;

    }

    public formatOfferOfHealthInsuranceCoverage(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.coverageCode ) {
                value += item.coverageCode + ' ';
            }

            if ( item.requiredContribution ) {
                value += item.requiredContribution + '\n';
            }

            if ( item.section4980HCode ) {
                value += item.section4980HCode + ' ';
            }

            if ( item.month ) {
                value += item.month + '\n';
            }

        }

        return value;

    }

    public formatOtherPara2(
        obj,
        wrap: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += `${pair.description}  ${pair.amount}` + '\n';

        }

        return value;

    }

    public formatSecurityDetail(
        obj
    ): string {

        return '+ security + detail';

    }

    public formatStateCode2(
        obj,
        i: number
    ): string {

        // console.log( obj );

        if ( obj == null ) return 'null';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].state;

        return value;

    }

    public formatStateCodeId2(
        obj,
        i: number
    ): string {

        // console.log( obj );

        if ( obj == null ) return 'null';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].state + '/' + obj[i - 1].stateTaxId;

        return value;

    }

    public formatStateId2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].stateTaxId;

        return value;

    }

    public formatStateIncome2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].stateIncome );

        return value;

    }

    public formatStateWH2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].stateTaxWithheld );

        return value;

    }

    public formatStateTaxWithholding(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.state ) {
                value += 'State ................ ' + item.state + '\n';
            }

            if ( item.stateTaxWithheld ) {
                value += 'State tax withheld ... ' + item.stateTaxWithheld + '\n';
            }

            if ( item.stateTaxId ) {
                value += 'State tax id ......... ' + item.stateTaxId + '\n';
            }

            if ( item.stateIncome ) {
                value += 'State income ......... ' + item.stateIncome + '\n';
            }

        }

        return value;

    }

    public formatStreetAddress(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        if ( obj.line1 ) {
            value += obj.line1;
        }

        return value;

    }

    public formatSuffix(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.suffix ) {
            value += obj.suffix;
        }

        return value;

    }

    public formatTelephoneNumberPlusExtension(
        obj
    ): string {

        return '+ telephone + extension';

    }

    public formatTimestamp(
        obj
    ): string {

        let value = '';

        if ( obj == null ) {
            return value;
        }

        if ( obj.length < 8 ) {
            return obj;
        }

        value = obj.substr( 4, 2 ) + '/' + obj.substr( 6, 2 ) + '/' + obj.substr( 0, 4 );

        return value;

    }

    public formatZip(
        obj
    ): string {

        let value: string = '';

        if ( obj.postalCode ) {

            if ( obj.postalCode.length === 9 ) {

                value = obj.substring(0, 5) + '-' + obj.substring(5, 9);

            } else {

                value = obj.postalCode;

            }

        }

        return value;

    }


    // ============================================
    // Security details items
    // ============================================
    public formatAccruedMarketDiscount(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].accruedMarketDiscount );

        return value;

    }

    public formatBasisReported(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].basisReported ? 'X' : '';

        return value;

    }

    public formatCheckboxOnForm8949SalesPrice(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].checkboxOnForm8949;

        return value;

    }

    public formatCollectible(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].collectible ? 'X' : '';

        return value;

    }

    public formatCostBasis(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].costBasis );

        return value;

    }

    public formatDateAcquired(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = this.formatTimestamp( obj[i - 1].dateAcquired );

        return value;

    }

    public formatDateOfSale(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = this.formatTimestamp( obj[i - 1].dateOfSale );

        return value;

    }

    public formatForeignAccountTaxCompliance(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].foreignAccountTaxCompliance ? 'X' : '';

        return value;

    }

    public formatGrossOrNet(
        obj,
        key,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].grossOrNet === key ? 'X' : '';

        return value;

    }

    public formatLongOrShort(
        obj,
        key,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].longOrShort === key ? 'X' : '';

        return value;

    }

    public formatLossNotAllowed(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].lossNotAllowed ? 'X' : '';

        return value;

    }

    public formatNoncoveredSecurity(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].noncoveredSecurity ? 'X' : '';

        return value;

    }

    public formatOrdinary(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].ordinary ? 'X' : '';

        return value;

    }

    public formatQof(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].qof ? 'X' : '';

        return value;

    }

    public formatSaleDescription(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = obj[i - 1].saleDescription;

        return value;

    }

    public formatSalesPrice(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].salesPrice );

        return value;

    }

    public formatWashSaleLossDisallowed(
        obj,
        i
    ) {
        let value: string = '';
        if ( obj == null ) return value;
        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].washSaleLossDisallowed );

        return value;

    }

}

