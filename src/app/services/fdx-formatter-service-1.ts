import { Injectable } from '@angular/core';
import { CodeAmount } from '../models/codeAmount';

@Injectable( {
    providedIn: 'root'
} )
export class FdxFormatterService1 {

    public format4(
        objType: string,
        obj1: any,
        obj2: any,
        other: any
    ): string {

        switch ( objType ) {

            case 'NamePhone':
                return '' + obj1 + ' ' + obj2;
                break;

            default:
                return '' + objType + '-4';

        }

    }

    public format3(
        objType: string,
        obj: any,
        elNum: any
    ): string {

        switch( objType ) {

            case 'CodeAmount':
                return this.formatCodeAmount2( obj, elNum );
                break;

            case 'CodeLetter':
                return this.formatCodeLetter2( obj, elNum );
                break;

            case 'Locality':
                return this.formatLocality2( obj, elNum );
                break;

            case 'LocalIncome':
                return this.formatLocalIncome2( obj, elNum );
                break;

            case 'LocalWH':
                return this.formatLocalWH2( obj, elNum );
                break;

            case 'OtherPara':
                return this.formatOtherPara2( obj, elNum );
                break;

            case 'StateCode':
                return this.formatStateCode2( obj, elNum );
                break;

            case 'StateCodeId':
                return this.formatStateCodeId2( obj, elNum );
                break;

            case 'StateId':
                return this.formatStateId2( obj, elNum );
                break;

            case 'StateIncome':
                return this.formatStateIncome2( obj, elNum );
                break;

            case 'StateWH':
                return this.formatStateWH2( obj, elNum );
                break;

            default:
                console.log( objType );
                return objType;

        }

    }

    public format(
        objType: string,
        obj: any
    ): string {

        switch( objType ) {

            case 'array':
                return this.formatArray( obj );
                break;

            case 'Address':
                return this.formatAddress( obj );
                break;

            case 'Boolean':
                return this.formatBoolean( obj );
                break;

            case 'CityStateZip':
                return this.formatCityStateZip( obj );
                break;

            case 'CodeAmount':
                return this.formatCodeAmount( obj );
                break;

            case 'DescriptionAmount':
                return this.formatDescriptionAmount( obj );
                break;

            case 'FirstInit':
                return this.formatFirstInit( obj );
                break;

            case 'HealthInsuranceCoveredIndividual':
                return this.formatHealthInsuranceCoveredIndividual( obj );
                break;

            case 'HealthInsuranceCoverage':
                return this.formatHealthInsuranceCoverage( obj );
                break;

            case 'HealthInsuranceMarketplaceCoveredIndividual':
                return this.formatHealthInsuranceMarketplaceCoveredIndividual( obj );
                break;

            case 'IndividualName':
                return this.formatIndividualName( obj );
                break;

            case 'integer':
                return this.formatInteger( obj );
                break;

            case 'LastName':
                return this.formatLastName( obj );
                break;

            case 'LocalTaxWithholding':
                return this.formatLocalTaxWithholding( obj );
                break;

            case 'MonthAmount':
                return this.formatMonthAmount( obj );
                break;

            case 'Name':
                return this.formatName( obj );
                break;

            case 'NameAddress':
                return this.formatNameAddress( obj );
                break;

            case 'NameAddressPhone':
            case 'NameEtc':
                return this.formatNameAddressPhone( obj );
                break;

            case 'number':
                return this.formatNumber( obj );
                break;

            case 'OfferOfHealthInsuranceCoverage':
                return this.formatOfferOfHealthInsuranceCoverage( obj );
                break;

            case 'SecurityDetail':
                return this.formatSecurityDetail( obj );
                break;

            case 'StateTaxWithholding':
                return this.formatStateTaxWithholding( obj );
                break;

            case 'StreetAddress':
                return this.formatStreetAddress( obj );
                break;

            case 'Suffix':
                return this.formatSuffix( obj );
                break;

            case 'TelephoneNumberPlusExtension':
                return this.formatTelephoneNumberPlusExtension( obj );
                break;

            case 'Timestamp':
                return this.formatTimestamp( obj );
                break;

            default:
                console.log( objType );
                return objType;

        }

    }

    public formatAddress(
        obj
    ): string {

        let value: string = '';

        if ( obj.line1 ) {
            value += obj.line1 + '\n';
        }

        if ( obj.line2 ) {
            value += obj.line2 + '\n';
        }

        // if ( obj.line3 ) {
        //     value += obj.line3 + '\n';
        // }

        const cityStateZip = this.formatCityStateZip( obj );
        if ( cityStateZip ) {
            value += cityStateZip;
        }

        return value;

    }

    public formatArray(
        obj
    ): string {

        return '+ array ';

    }

    public formatBoolean(
        obj
    ): string {

        if ( obj ) {
            return 'X';
        }

        return '';

    }

    public formatCityStateZip(
        obj
    ): string {

        let value: string = '';

        if ( obj.city ) {
            value += obj.city;
        }

        if ( obj.state ) {
            value += ', ' + obj.state;
        }

        const zip = this.formatZip( obj );
        if ( zip ) {
            value += ' ' + zip;
        }

        return value;

    }

    public formatCodeAmount(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += pair.code + ' ' + this.formatNumber( pair.amount );

        }

        return value;

    }

    public formatCodeAmount2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].amount );

        return value;

    }

    public formatCodeLetter2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].code;

        return value;

    }

    public formatDescriptionAmount(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += pair.description + ' ' + this.formatNumber( pair.amount );

        }

        return value;

    }

    public formatFirstInit(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.first ) {
            value += obj.first + ' ';
        }

        if ( obj.middle ) {
            value += obj.middle;
        }

        return value;

    }

    public formatHealthInsuranceCoverage(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.enrollmentPremium ) {
                value += item.enrollmentPremium + ' ';
            }

            if ( item.slcspPremium ) {
                value += item.slcspPremium + '\n';
            }

            if ( item.advancePremiumTaxCreditPayment ) {
                value += item.advancePremiumTaxCreditPayment + ' ';
            }

            if ( item.month ) {
                value += item.month + '\n';
            }

        }

        return value;

    }

    public formatHealthInsuranceCoveredIndividual(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.name ) {
                value += this.formatIndividualName( item.name ) + ' ';
            }

            if ( item.tin ) {
                value += item.tin + '\n';
            }

            if ( item.dateOfBirth ) {
                value += item.dateOfBirth + ' ';
            }

            if ( item.coveredAt12Months ) {
                value += item.coveredAt12Months + '\n';
            }

            if ( item.coveredMonths ) {
                for ( let mo of item.coveredMonths ) {
                    value += mo + '\n';
                }
            }

        }

        return value;

    }

    public formatHealthInsuranceMarketplaceCoveredIndividual(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.name ) {
                value += item.name + ' ';
            }

            if ( item.tin ) {
                value += item.tin + '\n';
            }

            if ( item.dateOfBirth ) {
                value += item.dateOfBirth + ' ';
            }

            if ( item.coveredAt12Months ) {
                value += item.coveredAt12Months + '\n';
            }

            if ( item.coveredMonths ) {
                for ( let mo of item.coveredMonths ) {
                    value += mo + '\n';
                }
            }

        }

        return value;

    }

    public formatIndividualName(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.first ) {
            value += obj.first + ' ';
        }

        if ( obj.middle ) {
            value += obj.middle + ' ';
        }

        if ( obj.last ) {
            value += obj.last + ' ';
        }

        if ( obj.suffix ) {
            value += obj.suffix;
        }

        return value;

    }

    public formatInteger(
        obj
    ): string {

        let value: string = '';

        value = String( obj );

        return value;

    }

    public formatLastName(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.last ) {
            value += obj.last;
        }

        return value;

    }

    public formatLocality2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].localityName;

        return value;

    }

    public formatLocalIncome2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].localIncome );

        return value;

    }

    public formatLocalWH2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].localTaxWithheld );

        return value;

    }

    public formatLocalTaxWithholding(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.localityName ) {
                value += 'Locality name ........ ' + item.localityName + '\n';
            }

            if ( item.localTaxWithheld ) {
                value += 'Local tax withheld ... ' + item.localTaxWithheld + '\n';
            }

            if ( item.state ) {
                value += 'State ................ ' + item.state + '\n';
            }

            if ( item.localIncome ) {
                value += 'Local income ......... ' + item.localIncome + '\n';
            }

        }

        return value;

    }

    public formatMonthAmount(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += `${pair.month}  ${pair.amount}`;

        }

        return value;

    }

    public formatName(
        obj
    ): string {

        let value: string = '';

        if ( obj.name1 ) {
            value += obj.name1;
        }

        return value;

    }

    public formatNameAddress(
        obj
    ): string {

        let value: string = '';

        if ( obj.name1 ) {
            value += obj.name1 + '\n';
        }

        if ( obj.name2 ) {
            value += obj.name2 + '\n';
        }

        const address: string = this.formatAddress( obj );
        if ( address ) {
            value += address;
        }

        return value;
    }

    public formatNameAddressPhone(
        obj
    ): string {

        let value: string = '';

        const address: string = this.formatNameAddress( obj );
        if ( address ) {
            value += address;
        }

        return value;

    }

    public formatNumber(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        value = String( obj.toFixed(2) );

        return value;

    }

    public formatOfferOfHealthInsuranceCoverage(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.coverageCode ) {
                value += item.coverageCode + ' ';
            }

            if ( item.requiredContribution ) {
                value += item.requiredContribution + '\n';
            }

            if ( item.section4980HCode ) {
                value += item.section4980HCode + ' ';
            }

            if ( item.month ) {
                value += item.month + '\n';
            }

        }

        return value;

    }

    public formatOtherPara2(
        obj,
        wrap: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        // Assume array for now
        for ( let pair of obj ) {

            value += `${pair.description}  ${pair.amount}` + '\n';

        }

        return value;

    }

    public formatSecurityDetail(
        obj
    ): string {

        return '+ security + detail';

    }

    public formatStateCode2(
        obj,
        i: number
    ): string {

        // console.log( obj );

        if ( obj == null ) return 'null';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].state;

        return value;

    }

    public formatStateCodeId2(
        obj,
        i: number
    ): string {

        // console.log( obj );

        if ( obj == null ) return 'null';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].state + '/' + obj[i - 1].stateTaxId;

        return value;

    }

    public formatStateId2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = obj[i - 1].stateTaxId;

        return value;

    }

    public formatStateIncome2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].stateIncome );

        return value;

    }

    public formatStateWH2(
        obj,
        i: number
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.length < i ) return value;

        value = this.formatNumber( obj[i - 1].stateTaxWithheld );

        return value;

    }

    public formatStateTaxWithholding(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        // Array assumed
        for ( let item of obj ) {

            if ( item.state ) {
                value += 'State ................ ' + item.state + '\n';
            }

            if ( item.stateTaxWithheld ) {
                value += 'State tax withheld ... ' + item.stateTaxWithheld + '\n';
            }

            if ( item.stateTaxId ) {
                value += 'State tax id ......... ' + item.stateTaxId + '\n';
            }

            if ( item.stateIncome ) {
                value += 'State income ......... ' + item.stateIncome + '\n';
            }

        }

        return value;

    }

    public formatStreetAddress(
        obj
    ): string {

        let value: string = '';

        if ( obj == null ) {
            return value;
        }

        if ( obj.line1 ) {
            value += obj.line1;
        }

        return value;

    }

    public formatSuffix(
        obj
    ): string {

        if ( obj == null ) return '';

        let value: string = '';

        if ( obj.suffix ) {
            value += obj.suffix;
        }

        return value;

    }

    public formatTelephoneNumberPlusExtension(
        obj
    ): string {

        return '+ telephone + extension';

    }

    public formatTimestamp(
        obj
    ): string {

        let value = '';

        if ( obj == null ) {
            return value;
        }

        if ( obj.length < 8 ) {
            return obj;
        }

        value = obj.substr( 4, 2 ) + '/' + obj.substr( 6, 2 ) + '/' + obj.substr( 0, 4 );

        return value;

    }

    public formatZip(
        obj
    ): string {

        let value: string = '';

        if ( obj.postalCode ) {

            if ( obj.postalCode.length === 9 ) {

                value = obj.substring(0, 5) + '-' + obj.substring(5, 9);

            } else {

                value = obj.postalCode;

            }

        }

        return value;

    }

}

