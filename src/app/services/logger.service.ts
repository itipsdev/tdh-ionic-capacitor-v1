import { Injectable } from '@angular/core';
import { isDevMode } from '@angular/core';
import * as Flatted from 'flatted';

@Injectable( {
    providedIn: 'root'
} )
export class LoggerService {

    constructor( ) {

    }

    // noinspection JSMethodCanBeStatic
    public log( message: string ): void {

        if ( isDevMode() ) {
            console.log( message );
        }

    }

    // noinspection JSMethodCanBeStatic
    public info( message: string ): void {

        if ( isDevMode() ) {
            console.log( `INFO: ${message}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public trace( message: string ): void {

        if ( isDevMode() ) {
            console.log( `TRACE: ${message}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public debug( message: string ): void {

        if ( isDevMode() ) {
            console.log( `DEBUG: ${message}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public debugWithLabel( message: string, label: string ): void {

        if ( isDevMode() ) {
            console.log( `DEBUG VALUE OF ${label}: ${message}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public debugAny( obj: any ): void {

        if ( isDevMode() ) {
            console.log( `DUMP OBJECT:`, obj );
        }

    }

    // noinspection JSMethodCanBeStatic
    public debugJson( obj: any ): void {

        if ( isDevMode() ) {
            const asJson: string = JSON.stringify( obj, null, 4 );
            console.log( `DUMP VALUE AS JSON: ${asJson}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public debugJsonWithLabel( obj: any, label: string ): void {

        if ( isDevMode() ) {
            const asJson: string = JSON.stringify( obj, null, 4 );
            console.log( `DUMP VALUE OF ${label} AS JSON: ${asJson}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public debugCircularJsonWithLabel( obj: any, label: string ) {

        if ( isDevMode() ) {
            const asJson: string = Flatted.stringify( obj, null, 4 );
            console.log( `DUMP VALUE OF ${label} AS JSON: ${asJson}` );
        }
    }

    public dump(
        label: string,
        value: string
    ): void {

        if ( isDevMode() ) {
            console.log( `DEBUG VALUE OF ${label}: ${value}` );
        }

    }

    // noinspection JSMethodCanBeStatic, JSUnusedGlobalSymbols
    public warn( message: string ): void {

        if ( isDevMode() ) {
            console.log( `WARN: ${message}` );
        }

    }

    // noinspection JSMethodCanBeStatic
    public severe( message: string ): void {

        if ( isDevMode() ) {
            console.log( `PROBLEM: ${message}` );
        } else {
            console.log( `PROBLEM: ${message}` ); // Different message? TODO
        }

    }

}
