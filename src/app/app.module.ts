// ===========================================================================================
// Angular
// ===========================================================================================
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PreloadAllModules, RouteReuseStrategy, RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

// ===========================================================================================
// Ionic
// ===========================================================================================
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

// ===========================================================================================
// Ionic Native
// ===========================================================================================
import { BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

// ===========================================================================================
// App
// ===========================================================================================
import { AppComponent } from './app.component';
import { DocComponent } from './components/doc/doc.component';
import { DocsComponent } from './components/docs/docs.component';
import { HelpComponent } from './components/help/help.component';
import { ScanDocComponent } from './components/scan-doc/scan-doc.component';
import { ScanLinkComponent } from './components/scan-link/scan-link.component';
import { ScanUpComponent } from './components/scan-up/scan-up.component';
import { SettingsComponent } from './components/settings/settings.component';
import { UploadComponent } from './components/upload/upload.component';
import { ViewScannedSubmitComponent } from './components/view-scanned-submit/view-scanned-submit.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { DocContentComponent } from './components/doc-content/doc-content.component';
import { Tax1041K1Component } from './components/doc-specific/tax1041k1/tax1041k1.component';
import { Tax1065K1Component } from './components/doc-specific/tax1065k1/tax1065k1.component';
import { Tax1095AComponent } from './components/doc-specific/tax1095a/tax1095a.component';
import { Tax1095BComponent } from './components/doc-specific/tax1095b/tax1095b.component';
import { Tax1095CComponent } from './components/doc-specific/tax1095c/tax1095c.component';
import { Tax1097BtcComponent } from './components/doc-specific/tax1097btc/tax1097btc.component';
import { Tax1098Component } from './components/doc-specific/tax1098/tax1098.component';
import { Tax1098CComponent } from './components/doc-specific/tax1098c/tax1098c.component';
import { Tax1098EComponent } from './components/doc-specific/tax1098e/tax1098e.component';
import { Tax1098TComponent } from './components/doc-specific/tax1098t/tax1098t.component';
import { Tax1099AComponent } from './components/doc-specific/tax1099a/tax1099a.component';
import { Tax1099BComponent } from './components/doc-specific/tax1099b/tax1099b.component';
import { Tax1099CComponent } from './components/doc-specific/tax1099c/tax1099c.component';
import { Tax1099CapComponent } from './components/doc-specific/tax1099cap/tax1099cap.component';
import { Tax1099DivComponent } from './components/doc-specific/tax1099div/tax1099div.component';
import { Tax1099GComponent } from './components/doc-specific/tax1099g/tax1099g.component';
import { Tax1099HComponent } from './components/doc-specific/tax1099h/tax1099h.component';
import { Tax1099IntComponent } from './components/doc-specific/tax1099int/tax1099int.component';
import { Tax1099KComponent } from './components/doc-specific/tax1099k/tax1099k.component';
import { Tax1099LtcComponent } from './components/doc-specific/tax1099ltc/tax1099ltc.component';
import { Tax1099MiscComponent } from './components/doc-specific/tax1099misc/tax1099misc.component';
import { Tax1099OidComponent } from './components/doc-specific/tax1099oid/tax1099oid.component';
import { Tax1099PatrComponent } from './components/doc-specific/tax1099patr/tax1099patr.component';
import { Tax1099QComponent } from './components/doc-specific/tax1099q/tax1099q.component';
import { Tax1099RComponent } from './components/doc-specific/tax1099r/tax1099r.component';
import { Tax1099SComponent } from './components/doc-specific/tax1099s/tax1099s.component';
import { Tax1099SaComponent } from './components/doc-specific/tax1099sa/tax1099sa.component';
import { Tax1120SK1Component } from './components/doc-specific/tax1120sk1/tax1120sk1.component';
import { Tax2439Component } from './components/doc-specific/tax2439/tax2439.component';
import { Tax5498Component } from './components/doc-specific/tax5498/tax5498.component';
import { Tax5498EsaComponent } from './components/doc-specific/tax5498esa/tax5498esa.component';
import { Tax5498SaComponent } from './components/doc-specific/tax5498sa/tax5498sa.component';
import { TaxW2Component } from './components/doc-specific/taxw2/taxw2.component';
import { TaxW2GComponent } from './components/doc-specific/taxw2g/taxw2g.component';
import { ScanErrorComponent } from './components/scan-error/scan-error.component';
import { BuildNumComponent } from './components/build-num/build-num.component';
import { FormsModule } from '@angular/forms';
import { DocFacsimileComponent } from './components/doc-facsimile/doc-facsimile.component';
import { DocFacsimileContentComponent } from './components/doc-facsimile-content/doc-facsimile-content.component';
import { Tax1041K1FacsimileComponent } from './components/facsimiles/tax1041k1-facsimile/tax1041k1-facsimile.component';
import { Tax1065K1FacsimileComponent } from './components/facsimiles/tax1065k1-facsimile/tax1065k1-facsimile.component';
import { Tax1095AFacsimileComponent } from './components/facsimiles/tax1095a-facsimile/tax1095a-facsimile.component';
import { Tax1095BFacsimileComponent } from './components/facsimiles/tax1095b-facsimile/tax1095b-facsimile.component';
import { Tax1095CFacsimileComponent } from './components/facsimiles/tax1095c-facsimile/tax1095c-facsimile.component';
import { Tax1097BtcFacsimileComponent } from './components/facsimiles/tax1097btc-facsimile/tax1097btc-facsimile.component';
import { Tax1098FacsimileComponent } from './components/facsimiles/tax1098-facsimile/tax1098-facsimile.component';
import { Tax1098CFacsimileComponent } from './components/facsimiles/tax1098c-facsimile/tax1098c-facsimile.component';
import { Tax1098EFacsimileComponent } from './components/facsimiles/tax1098e-facsimile/tax1098e-facsimile.component';
import { Tax1098TFacsimileComponent } from './components/facsimiles/tax1098t-facsimile/tax1098t-facsimile.component';
import { Tax1099BFacsimileComponent } from './components/facsimiles/tax1099b-facsimile/tax1099b-facsimile.component';
import { Tax1099CFacsimileComponent } from './components/facsimiles/tax1099c-facsimile/tax1099c-facsimile.component';
import { Tax1099CapFacsimileComponent } from './components/facsimiles/tax1099cap-facsimile/tax1099cap-facsimile.component';
import { Tax1099DivFacsimileComponent } from './components/facsimiles/tax1099div-facsimile/tax1099div-facsimile.component';
import { Tax1099GFacsimileComponent } from './components/facsimiles/tax1099g-facsimile/tax1099g-facsimile.component';
import { Tax1099HFacsimileComponent } from './components/facsimiles/tax1099h-facsimile/tax1099h-facsimile.component';
import { Tax1099IntFacsimileComponent } from './components/facsimiles/tax1099int-facsimile/tax1099int-facsimile.component';
import { Tax1099KFacsimileComponent } from './components/facsimiles/tax1099k-facsimile/tax1099k-facsimile.component';
import { Tax1099LtcFacsimileComponent } from './components/facsimiles/tax1099ltc-facsimile/tax1099ltc-facsimile.component';
import { Tax1099MiscFacsimileComponent } from './components/facsimiles/tax1099misc-facsimile/tax1099misc-facsimile.component';
import { Tax1099OidFacsimileComponent } from './components/facsimiles/tax1099oid-facsimile/tax1099oid-facsimile.component';
import { Tax1099PatrFacsimileComponent } from './components/facsimiles/tax1099patr-facsimile/tax1099patr-facsimile.component';
import { Tax1099QFacsimileComponent } from './components/facsimiles/tax1099q-facsimile/tax1099q-facsimile.component';
import { Tax1099RFacsimileComponent } from './components/facsimiles/tax1099r-facsimile/tax1099r-facsimile.component';
import { Tax1099SFacsimileComponent } from './components/facsimiles/tax1099s-facsimile/tax1099s-facsimile.component';
import { Tax1099SaFacsimileComponent } from './components/facsimiles/tax1099sa-facsimile/tax1099sa-facsimile.component';
import { Tax1120SK1FacsimileComponent } from './components/facsimiles/tax1120sk1-facsimile/tax1120sk1-facsimile.component';
import { Tax2439FacsimileComponent } from './components/facsimiles/tax2439-facsimile/tax2439-facsimile.component';
import { Tax5498FacsimileComponent } from './components/facsimiles/tax5498-facsimile/tax5498-facsimile.component';
import { Tax5498EsaFacsimileComponent } from './components/facsimiles/tax5498esa-facsimile/tax5498esa-facsimile.component';
import { Tax5498SaFacsimileComponent } from './components/facsimiles/tax5498sa-facsimile/tax5498sa-facsimile.component';
import { TaxW2FacsimileComponent } from './components/facsimiles/taxw2-facsimile/taxw2-facsimile.component';
import { TaxW2GFacsimileComponent } from './components/facsimiles/taxw2g-facsimile/taxw2g-facsimile.component';
import { Tax1099AFacsimileComponent } from './components/facsimiles/tax1099a-facsimile/tax1099a-facsimile.component';
import { ScanSimComponent } from './components/scan-sim/scan-sim.component';

// ===========================================================================================
// Routes
// ===========================================================================================
const routes: Routes = [
    {
        path: '',
        component: WelcomeComponent,
        pathMatch: 'full'
    },
    {
        path: 'doc/:docId',
        component: DocComponent,
    },
    {
        path: 'doc-facsimile/:docId',
        component: DocFacsimileComponent,
    },
    {
        path: 'docs',
        component: DocsComponent
    },
    {
        path: 'help',
        component: HelpComponent
    },
    {
        path: 'scan-doc',
        component: ScanDocComponent
    },
    {
        path: 'scan-error',
        component: ScanErrorComponent
    },
    {
        path: 'scan-link',
        component: ScanLinkComponent
    },
    {
        path: 'scan-up',
        component: ScanUpComponent
    },
    {
        path: 'scan-sim',
        component: ScanSimComponent
    },
    {
        path: 'settings',
        component: SettingsComponent
    },
    {
        path: 'upload',
        component: UploadComponent
    },
    {
        path: 'view-scanned-submit',
        component: ViewScannedSubmitComponent
    },
    {
        path: 'welcome',
        component: WelcomeComponent
    },
];


@NgModule( {
    declarations: [
        AppComponent,
        BuildNumComponent,
        DocComponent,
        DocContentComponent,
        DocFacsimileComponent,
        DocFacsimileContentComponent,
        DocsComponent,
        HelpComponent,
        ScanDocComponent,
        ScanErrorComponent,
        ScanLinkComponent,
        ScanSimComponent,
        ScanUpComponent,
        SettingsComponent,
        UploadComponent,
        ViewScannedSubmitComponent,
        WelcomeComponent,
        Tax1041K1Component,
        Tax1065K1Component,
        Tax1095AComponent,
        Tax1095BComponent,
        Tax1095CComponent,
        Tax1097BtcComponent,
        Tax1098Component,
        Tax1098CComponent,
        Tax1098EComponent,
        Tax1098TComponent,
        Tax1099AComponent,
        Tax1099BComponent,
        Tax1099CComponent,
        Tax1099CapComponent,
        Tax1099DivComponent,
        Tax1099GComponent,
        Tax1099HComponent,
        Tax1099IntComponent,
        Tax1099KComponent,
        Tax1099LtcComponent,
        Tax1099MiscComponent,
        Tax1099OidComponent,
        Tax1099PatrComponent,
        Tax1099QComponent,
        Tax1099RComponent,
        Tax1099SComponent,
        Tax1099SaComponent,
        Tax1120SK1Component,
        Tax2439Component,
        Tax5498Component,
        Tax5498EsaComponent,
        Tax5498SaComponent,
        TaxW2Component,
        TaxW2GComponent,
        Tax1041K1FacsimileComponent,
        Tax1065K1FacsimileComponent,
        Tax1095AFacsimileComponent,
        Tax1095BFacsimileComponent,
        Tax1095CFacsimileComponent,
        Tax1097BtcFacsimileComponent,
        Tax1098FacsimileComponent,
        Tax1098CFacsimileComponent,
        Tax1098EFacsimileComponent,
        Tax1098TFacsimileComponent,
        Tax1099AFacsimileComponent,
        Tax1099BFacsimileComponent,
        Tax1099CFacsimileComponent,
        Tax1099CapFacsimileComponent,
        Tax1099DivFacsimileComponent,
        Tax1099GFacsimileComponent,
        Tax1099HFacsimileComponent,
        Tax1099IntFacsimileComponent,
        Tax1099KFacsimileComponent,
        Tax1099LtcFacsimileComponent,
        Tax1099MiscFacsimileComponent,
        Tax1099OidFacsimileComponent,
        Tax1099PatrFacsimileComponent,
        Tax1099QFacsimileComponent,
        Tax1099RFacsimileComponent,
        Tax1099SFacsimileComponent,
        Tax1099SaFacsimileComponent,
        Tax1120SK1FacsimileComponent,
        Tax2439FacsimileComponent,
        Tax5498FacsimileComponent,
        Tax5498EsaFacsimileComponent,
        Tax5498SaFacsimileComponent,
        TaxW2FacsimileComponent,
        TaxW2GFacsimileComponent

    ],
    entryComponents: [

    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        IonicModule.forRoot( ),
        RouterModule.forRoot(
            routes,
            { preloadingStrategy: PreloadAllModules }
        )
    ],
    providers: [
        BarcodeScanner,
        StatusBar,
        SplashScreen,
        {
            provide: RouteReuseStrategy,
            useClass: IonicRouteStrategy
        }
    ],
    bootstrap: [
        AppComponent
    ]
} )
export class AppModule {

}
