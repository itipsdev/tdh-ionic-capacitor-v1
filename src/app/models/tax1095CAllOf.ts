/**
 * FDX V4.0
 * Financial Data Exchange V4.0 API
 *
 * The version of the OpenAPI document: 4.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { IndividualName } from './individualName';
import { HealthInsuranceCoveredIndividual } from './healthInsuranceCoveredIndividual';
import { Address } from './address';
import { OfferOfHealthInsuranceCoverage } from './offerOfHealthInsuranceCoverage';
import { NameAddressPhone } from './nameAddressPhone';


export interface Tax1095CAllOf { 
    employeeName?: IndividualName;
    /**
     * Box 2, Social security number (SSN)
     */
    tin?: string;
    employeeAddress?: Address;
    employerNameAddressPhone?: NameAddressPhone;
    /**
     * Box 8, Employer identification number (EIN)
     */
    employerId?: string;
    /**
     * true or false
     */
    selfInsuredCoverage?: boolean;
    /**
     * Employee Offer of Coverage
     */
    offersOfCoverage?: Array<OfferOfHealthInsuranceCoverage>;
    /**
     * Plan Start Month
     */
    planStartMonth?: number;
    /**
     * Covered Individuals
     */
    coveredIndividuals?: Array<HealthInsuranceCoveredIndividual>;
}

