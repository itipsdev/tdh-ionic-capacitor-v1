export interface TaxDocSubmissionWrapper {

    // Obsolete ? 2019

    taxYear?: string;

    taxFormId?: string;

    taxFormAsJson: string;

    issuerTaxId?: string;

    issuerName?: string;

}
